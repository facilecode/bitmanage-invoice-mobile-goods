package facile.Master;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;

import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.prefManager;


public class Shipping_Address extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    prefManager prefs;
    ArrayList<HashMap<String, String>> addaddressHashMap;

    EditText edt_address,edt_address1,edt_city,edt_pincode,edt_state,edt_country;
    String  str_address,str_address1,str_city,str_pincode,str_state,str_country;
    Button btn_add;

    private Shipping_Address.OnFragmentInteractionListener mListener;
    Invoicedatabase invoicedatabase;

    public Shipping_Address() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Invoice_list.
     */
    // TODO: Rename and change types and number of parameters
    public static Shipping_Address newInstance(String param1, String param2) {
        Shipping_Address fragment = new Shipping_Address();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final  View view =  inflater.inflate(R.layout.shipping_address, container, false);

         invoicedatabase=new Invoicedatabase(getContext());
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_shippingaddress));
        prefs=new prefManager(getContext());
        edt_address=(EditText)view.findViewById(R.id.edt_address);
        edt_address1=(EditText)view.findViewById(R.id.edt_address1);
        edt_city=(EditText)view.findViewById(R.id.edt_city);
        edt_state=(EditText)view.findViewById(R.id.edt_state);
        edt_country=(EditText)view.findViewById(R.id.edt_country);
        edt_pincode=(EditText)view.findViewById(R.id.edt_pincode);
        btn_add=(Button)view.findViewById(R.id.btn_add);


        Bundle bundle = this.getArguments();
        if(bundle!=null) {
                str_address = bundle.getString("shipping_address");
                str_address1 = bundle.getString("shipping_address1");
                str_city = bundle.getString("str_city");
                str_state = bundle.getString("str_state");
                str_pincode = bundle.getString("str_pincode");
                str_country = bundle.getString("str_country");
                edt_address.setText(str_address);
                edt_address1.setText(str_address1);
                edt_country.setText(str_country);
                edt_pincode.setText(str_pincode);
                edt_state.setText(str_state);
                edt_city.setText(str_city);
        }

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_address=edt_address.getText().toString();
                str_address1=edt_address1.getText().toString();
                str_city=edt_city.getText().toString();
                str_state=edt_state.getText().toString();
                str_pincode=edt_pincode.getText().toString();
                str_country=edt_country.getText().toString();

                  if (TextUtils.isEmpty(str_address)) {
                    edt_address.requestFocus();
                    edt_address.setError("customer Address can't be empty");

                    return;
                } else if (TextUtils.isEmpty(str_city)) {
                    edt_city.requestFocus();
                    edt_city.setError("customer City can't be empty");

                    return;
                } else if (TextUtils.isEmpty(str_pincode)) {
                    edt_city.requestFocus();
                    edt_city.setError("customer Pincode can't be empty");

                    return;
                }
                  else if (TextUtils.isEmpty(str_state)) {
                      edt_state.requestFocus();
                      edt_state.setError("customer Pincode can't be empty");

                      return;
                  }
                  else if (TextUtils.isEmpty(str_country)) {
                      edt_country.requestFocus();
                      edt_country.setError("customer Pincode can't be empty");

                      return;
                  }
                else{


                      invoicedatabase.deleteaaddshippingaddress();

/*    *//*   prefs.setShippingAddress1(str_address);
        prefs.setShippingAddress2(str_address1);
       prefs.setShippingCity(str_city);
       prefs.setShippingPincode(str_pincode);
       prefs.setShippingState(str_state);
       prefs.setShippingCountry(str_country);




*//*


        FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                    *//*  Intent intent = new Intent();
                      intent.putExtra("SHIPPPING_ADDRESS1", str_address);

                      intent.putExtra("SHIPPPING_ADDRESS2", str_address1);

*//*
                   *//* getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);*//*
                      getFragmentManager().popBackStack();

                   *//*   bundle.putString("SHIPPPING_ADDRESS1", str_address);
                      bundle.putString("SHIPPING_ADDRESS2",str_address1);
                      bundle.putString("SHIPPING_CITY", str_city);
                      bundle.putString("SHIPPING_COUNTRY", str_pincode);
                      bundle.putString("SHIPPING_STATE", str_state);
                      bundle.putString("SHIPPING_PINCODE", str_state);
                    *//*
                      *//*fragmentManager.popBackStack();*/
                      addaddressHashMap = new ArrayList<HashMap<String, String>>();
                      HashMap<String,String> addressmap=new HashMap<String, String>();
                      addressmap.put("shipping_id","1");
                      addressmap.put("shipping_address",str_address);
                      addressmap.put("shipping_address1",str_address1);
                      addressmap.put("shipping_city",str_city);
                      addressmap.put("shipping_state",str_state);
                      addressmap.put("shipping_country",str_country);
                      addressmap.put("shipping_pincode",str_pincode);
                      addaddressHashMap.add(addressmap);

                      invoicedatabase.insertaddshippingaddress(addaddressHashMap);


                      FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                      fragmentManager.popBackStack();



                  }
            }
        });



        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
