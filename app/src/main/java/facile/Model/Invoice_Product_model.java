package facile.Model;

/**
 * Created by pradeep on 4/3/18.
 */

public class Invoice_Product_model {

    public String id;
    public String productcode;
    public String productdesc;
    public String unit;
    public String hsncode;
    public String gstrate;
    public String cess;
    public String quantity;
    public String amount;
    public String discount;
    public String discountype;
    public String grossamount;
    public String cgst;
    public String sgst;
    public String igst;
    public String cgst_percent;
    public String sgst_percent;
    public String igst_percent;
    public String tat;
 }
