package facile.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pradeep on 11/4/18.
 */

public class Credit_note_details  implements Parcelable {
    public String invoice_id;
    public String credit_note_date;
    public String credit_note_no;
    public String credit_note_p_no;
    public String invoice_no;
    public String str_dateval;

    public Credit_note_details(String invoice_id, String credit_note_date, String credit_note_no, String credit_note_p_no,String invoice_no, String dateval){
        this.invoice_id = invoice_id;
        this.credit_note_date = credit_note_date;
        this.credit_note_no = credit_note_no;
        this.credit_note_p_no = credit_note_p_no;
        this.invoice_no=invoice_no;

        this.str_dateval=dateval;
    }
    public Credit_note_details(Parcel in) {
        invoice_id = in.readString();
        credit_note_date = in.readString();
        credit_note_no = in.readString();
        credit_note_p_no = in.readString();
        invoice_no=in.readString();

        str_dateval=in.readString();
    }
    public int describeContents() {
        return 0;
    }
    public void writeToParcel(Parcel dest,int flag) {
        dest.writeString(invoice_id);
        dest.writeString(credit_note_date);
        dest.writeString(credit_note_no);
        dest.writeString(credit_note_p_no);
        dest.writeString(invoice_no);

        dest.writeString(str_dateval);
    }

    public static final Parcelable.Creator<Credit_note_details> CREATOR = new Parcelable.Creator<Credit_note_details>() {

        public Credit_note_details createFromParcel(Parcel in) {
            return new Credit_note_details(in);
        }

        public Credit_note_details[] newArray(int size) {
            return new Credit_note_details[size];
        }

    };


}


