package facile.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pradeep on 10/3/18.
 */

public class Invoice_details implements Parcelable{
        public String sales_type;
        public String invoice_date;
        public String transport_mode;
        public String reverse_charge;
        public String invoice_no;
    public String p_invoice_no_s;
    public String person_type;

        public String str_dateval;

    public Invoice_details(String sales_type, String invoice_date, String transport_mode, String reverse_charge,String invoice_no,String p_invoice_no_s, String dateval,String person_type){
        this.sales_type = sales_type;
        this.invoice_date = invoice_date;
        this.transport_mode = transport_mode;
        this.reverse_charge = reverse_charge;
        this.invoice_no=invoice_no;
        this.p_invoice_no_s=p_invoice_no_s;
        this.str_dateval=dateval;
        this.person_type=person_type;
    }
   public Invoice_details(Parcel in) {
       sales_type = in.readString();
       invoice_date = in.readString();
       transport_mode = in.readString();
       reverse_charge = in.readString();
       invoice_no=in.readString();
       p_invoice_no_s=in.readString();
       str_dateval=in.readString();
       person_type=in.readString();
   }
    public int describeContents() {
        return 0;
    }
    public void writeToParcel(Parcel dest,int flag) {
        dest.writeString(sales_type);
        dest.writeString(invoice_date);
        dest.writeString(transport_mode);
        dest.writeString(reverse_charge);
        dest.writeString(invoice_no);
        dest.writeString(p_invoice_no_s);
        dest.writeString(str_dateval);
        dest.writeString(person_type);
   }

    public static final Parcelable.Creator<Invoice_details> CREATOR = new Parcelable.Creator<Invoice_details>() {

        public Invoice_details createFromParcel(Parcel in) {
            return new Invoice_details(in);
        }

        public Invoice_details[] newArray(int size) {
            return new Invoice_details[size];
        }

    };


}
