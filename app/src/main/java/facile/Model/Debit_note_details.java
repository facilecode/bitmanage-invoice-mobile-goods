package facile.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pradeep on 11/4/18.
 */

public class Debit_note_details  implements Parcelable {
    public String invoice_id;
    public String debit_note_date;
    public String debit_note_no;
    public String debit_note_p_no;
    public String invoice_no;
    public String str_dateval;

    public Debit_note_details(String invoice_id, String debit_note_date, String debit_note_no, String debit_note_p_no,String invoice_no, String dateval){
        this.invoice_id = invoice_id;
        this.debit_note_date = debit_note_date;
        this.debit_note_no = debit_note_no;
        this.debit_note_p_no = debit_note_p_no;
        this.invoice_no=invoice_no;

        this.str_dateval=dateval;
    }
    public Debit_note_details(Parcel in) {
        invoice_id = in.readString();
        debit_note_date = in.readString();
        debit_note_no = in.readString();
        debit_note_p_no = in.readString();
        invoice_no=in.readString();

        str_dateval=in.readString();
    }
    public int describeContents() {
        return 0;
    }
    public void writeToParcel(Parcel dest,int flag) {
        dest.writeString(invoice_id);
        dest.writeString(debit_note_date);
        dest.writeString(debit_note_no);
        dest.writeString(debit_note_p_no);
        dest.writeString(invoice_no);

        dest.writeString(str_dateval);
    }

    public static final Parcelable.Creator<Debit_note_details> CREATOR = new Parcelable.Creator<Debit_note_details>() {

        public Debit_note_details createFromParcel(Parcel in) {
            return new Debit_note_details(in);
        }

        public Debit_note_details[] newArray(int size) {
            return new Debit_note_details[size];
        }

    };


}


