package facile.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pradeep on 18/7/18.
 */

public class Debit_note_purchase_details implements Parcelable {
    public String str_dateval;
    public String debit_note_no;
    public String invoice_no;


    public Debit_note_purchase_details( String debit_note_no,String invoice_no, String dateval){
        this.str_dateval = dateval;
        this.debit_note_no = debit_note_no;
        this.invoice_no=invoice_no;

    }
    public Debit_note_purchase_details(Parcel in) {

        debit_note_no = in.readString();
        invoice_no=in.readString();


        str_dateval=in.readString();
    }
    public int describeContents() {
        return 0;
    }
    public void writeToParcel(Parcel dest,int flag) {

        dest.writeString(debit_note_no);

        dest.writeString(str_dateval);
        dest.writeString(invoice_no);

    }

    public static final Parcelable.Creator<Debit_note_purchase_details> CREATOR = new Parcelable.Creator<Debit_note_purchase_details>() {

        public Debit_note_purchase_details createFromParcel(Parcel in) {
            return new Debit_note_purchase_details(in);
        }

        public Debit_note_purchase_details[] newArray(int size) {
            return new Debit_note_purchase_details[size];
        }

    };

}
