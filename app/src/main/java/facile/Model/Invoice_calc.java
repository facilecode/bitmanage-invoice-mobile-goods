package facile.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by pradeep on 10/3/18.
 */

public class Invoice_calc implements Parcelable {


    public String total_before_tax;
    public String freight;
    public String other_charges;
    public String total_after_tax;
    public String cgst;
    public String sgst;
    public String igst;


    public Invoice_calc(Parcel in) {
        freight = in.readString();
        other_charges = in.readString();
        total_before_tax = in.readString();
        total_after_tax = in.readString();
        cgst = in.readString();
        sgst = in.readString();
        igst = in.readString();
    }

    public Invoice_calc(String freight, String other_charges, String cgst, String sgst,String igst,
                        String total_before_tax,String total_after_tax){
        this.freight = freight;
        this.other_charges = other_charges;
        this.cgst = cgst;
        this.sgst = sgst;
        this.igst=igst;
        this.total_before_tax=total_before_tax;
        this.total_after_tax=total_after_tax;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(freight);
        dest.writeString(other_charges);
        dest.writeString(total_before_tax);
        dest.writeString(total_after_tax);
        dest.writeString(cgst);
        dest.writeString(sgst);
        dest.writeString(igst);

    }

    public static final Parcelable.Creator<Invoice_calc> CREATOR = new Parcelable.Creator<Invoice_calc>() {

        public Invoice_calc createFromParcel(Parcel in) {
            return new Invoice_calc(in);
        }

        public Invoice_calc[] newArray(int size) {
            return new Invoice_calc[size];
        }

    };



}
