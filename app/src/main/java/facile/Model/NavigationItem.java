package facile.Model;

/**
 * Created by Pradeep on 18-02-2016.
 */
public class NavigationItem {

    private boolean showNotify;
    private String title;


  private int titleicon;

    public NavigationItem() {

    }

    public NavigationItem(boolean showNotify, String title, int titleicon) {
        this.showNotify = showNotify;
        this.title = title;
      this.titleicon=titleicon;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
  public int getTitleicon() {
        return titleicon;
    }

    public void setTitleicon(int titleicon) {
        this.titleicon = titleicon;
    }

}
