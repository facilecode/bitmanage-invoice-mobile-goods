package facile.invoice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.AutoCompleteCusomerAdapter;
import facile.Adapter.TransportlistArrayAdapter;
import facile.Adapter.additemadapter;
import facile.Model.Customer_model;
import facile.Model.Invoice_Product_model;
import facile.Model.Invoice_calc;
import facile.Model.Invoice_details;
import facile.Model.invoice_customer_model;
import facile.localdb.Invoicedatabase;
import facile.pdf.ImagetoPdf;
import facile.util.Constants;
import facile.util.SeparatorDecoration;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 12/2/18.
 */

public class Invoice_edit extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView toasttext;
    String str_date;
    View layouttoast;
    Button submit, additem,btn_total;
    EditText edt_inv_date, edt_address,edt_address1,edt_pincode,edt_city,edt_state;;
    TextView     tv_invoiceno,txt_gstno;
    AutoCompleteCusomerAdapter adapter;
    additemadapter Itemadapter;
    RecyclerView mRecyclerView;
    Invoicedatabase invoicedb;
    Spinner spn_transport;
    ArrayList<Invoice_Product_model> invoicproductemodel;
    private RecyclerView.LayoutManager mLayoutManager;
    String str_transportmode;
    String str_customername,str_gstno,str_address;
    RadioGroup radioGroup;
    String sales="x",transport_mode="1";
    CheckBox chk_rc,chk_shippingaddress;
    EditText edt_freight,edt_others;
    prefManager prefs;
    String str_igst="N";
    Double freight=0.0,others=0.0,freightbt=0.0,othersbt=0.0;
    String Str_customer_name,gst_no,address,address1,phone_no,mail_id,city,state,pincode,state_pos,
            s_address,s_address1,s_phone_no,s_mail_id,s_city,s_state,s_pincode,str_invoiceno;
    RequestQueue requestQueue;
    public static final String UTF8_BOM = "\uFEFF";
    RadioButton rb_cash,rb_credit;
    String  str_dateval;
    ProgressDialog progressDialog;
    String url;
     double temp;
    String first="0";
    private Invoice_edit.OnFragmentInteractionListener mListener;
    ArrayList<HashMap<String, String>> addproductHashMap;
    String str_getinvoiceid;
    AutoCompleteTextView edt_customername;
    String str_freight;
    String str_others;
    String total_before_tax;
    String total_after_tax;
    String str_cgst, str_sgst, str_igstval;
    String str_invno,str_invno_ps;
    TextView tv_totalaftertax,tv_tax,tv_totalbeforetax;
    TextView txt_totbeforetax,txt_totaftertax,txt_tax;
    String business_type;

    public Invoice_edit() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Invoice_edit newInstance(String param1, String param2) {
        Invoice_edit fragment = new Invoice_edit();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.invoice_main, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_invoice));
        prefs = new prefManager(getContext());
        edt_inv_date = (EditText) view.findViewById(R.id.edt_invoicedate);
        txt_gstno = (TextView) view.findViewById(R.id.txt_gstno);
        txt_gstno.setText(gst_no);
        edt_address = (EditText) view.findViewById(R.id.edt_address);
        chk_shippingaddress=(CheckBox)view.findViewById(R.id.chk_shippingaddress);
        additem = (Button) view.findViewById(R.id.btn_additem);
        spn_transport=(Spinner)view.findViewById(R.id.spn_transport);
        tv_invoiceno=(TextView)view.findViewById(R.id.tv_invoicenumber);
        btn_total = (Button) view.findViewById(R.id.btn_total);
        tv_totalaftertax=(TextView)view.findViewById(R.id.tv_totalaftertax);
        tv_totalbeforetax=(TextView)view.findViewById(R.id.tv_totalbeforetax);
        tv_tax=(TextView)view.findViewById(R.id.tv_tax);
        txt_totaftertax=(TextView)view.findViewById(R.id.txt_totalaftertax);
        txt_totbeforetax=(TextView)view.findViewById(R.id.txt_totalbeforetax);
        txt_tax=(TextView)view.findViewById(R.id.txt_tax);
        tv_invoiceno.setText(str_invno_ps);
        radioGroup=(RadioGroup)view.findViewById(R.id.radioGroup);
        rb_cash=(RadioButton)view.findViewById(R.id.rb_cash);
        rb_credit=(RadioButton)view.findViewById(R.id.rb_credit);
        edt_address1=(EditText)view.findViewById(R.id.edt_address1);
        edt_city=(EditText)view.findViewById(R.id.edt_city);
        edt_pincode=(EditText)view.findViewById(R.id.edt_pincode);
        edt_state=(EditText)view.findViewById(R.id.edt_state);
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_invoiceitems);
        chk_rc=(CheckBox)view.findViewById(R.id.chk_reversecharge);
        edt_freight=(EditText)view.findViewById(R.id.edt_freight);
        edt_others=(EditText)view.findViewById(R.id.edt_others);
        if(prefs.getBusinessNature().equals("2")){
            if(prefs.getServiceType().equals("1")){

                additem.setText(R.string.add_service);
            }
            else   if(prefs.getServiceType().equals("2")){
                additem.setText(R.string.add_service);

            }
            else  if(prefs.getServiceType().equals("3")){
                additem.setText(R.string.add_service);
            }



        }
        Bundle bundle = this.getArguments();
        if(first.equals("0")) {
        if (bundle != null) {
                str_getinvoiceid = bundle.getString("invoice_id");
                getinvoicedetails(view);
            chk_shippingaddress.setVisibility(View.GONE);
                first = "1";
            }
        }
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        SeparatorDecoration decoration = new SeparatorDecoration(getContext(), Color.GRAY,0.5f);
        mRecyclerView.addItemDecoration(decoration);
        edt_customername = (AutoCompleteTextView) view.findViewById(R.id.edt_customername);
        edt_customername.setThreshold(1);
        invoicedb = new Invoicedatabase(getContext());
        invoicproductemodel=new ArrayList<Invoice_Product_model>();
        final TransportlistArrayAdapter adapter1 = new TransportlistArrayAdapter(getContext(),
                R.layout.dealer_list_row, invoicedb.gettransportmode());
        spn_transport.setAdapter(adapter1);
        spn_transport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                /*str_transportmode=((TextView) v.findViewById(R.id.tv_dealerid)).getText().toString();*/
                str_transportmode=String.valueOf(position+1);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.rb_cash:
                        // switch to fragment 1
                        sales="1";
                        break;
                    case R.id.rb_credit:
                        // Fragment 2
                        sales="2";
                        break;
                }
            }
        });



        Itemadapter = new additemadapter(invoicedb.getaddproductitems(),getActivity());
        mRecyclerView.setAdapter(Itemadapter);
        ((additemadapter) Itemadapter).setOnItemClickListener(new additemadapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {

                Additem_fragment fragment = new Additem_fragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("prod_code",invoicedb.getaddproductitems().get(position).productcode);
                bundle.putString("prod_desc",invoicedb.getaddproductitems().get(position).productdesc);
                bundle.putString("prod_fees",invoicedb.getaddproductitems().get(position).amount);
                bundle.putString("prod_qty",invoicedb.getaddproductitems().get(position).quantity);
                bundle.putString("prod_disc",invoicedb.getaddproductitems().get(position).discount);
                bundle.putString("prod_disc_type",invoicedb.getaddproductitems().get(position).discountype);
                bundle.putString("prod_unit",invoicedb.getaddproductitems().get(position).unit);
                bundle.putString("gst_rate",invoicedb.getaddproductitems().get(position).gstrate);
                bundle.putString("Igst",str_igst);
                bundle.putString("type","IE");
                bundle.putString("business_type",business_type);
                    /*    bundle.putString("ITEM_CODE", );
                bundle.putString("ITEM_DESC", invoicedb.getcustomerdetails().get(position).gst_no);
                bundle.putString("SP", invoicedb.getcustomerdetails().get(position).address);
                bundle.putString("QTY", invoicedb.getcustomerdetails().get(position).city);
                bundle.putString("DISCOUNT", invoicedb.getcustomerdetails().get(position).pincode);*/
                fragment.setArguments(bundle);
                fragmentTransaction.commit();


            }
        });


       adapter = new AutoCompleteCusomerAdapter(getContext(),R.layout.autocomplete_row,invoicedb.getcustomerdetails());
        edt_customername.setAdapter(adapter);
        edt_customername.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                chk_shippingaddress.setVisibility(View.VISIBLE);
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                Customer_model model = (Customer_model) view.getTag();
                txt_gstno.setText(model.gst_no);
                edt_customername.setText(model.customer_name);
                edt_address.setText(model.address);
                if(model.state_pos.equals(prefs.getState())){
                    str_igst="N";
                }
                else{
                    str_igst="Y";
                }
                Str_customer_name=model.customer_name;
                gst_no=model.gst_no;
                address=model.address;
                phone_no=model.phone_no;
                mail_id=model.mail_id;
                city=model.city;
                state=model.state;
                pincode=model.pincode;
                state_pos=model.state_pos;

                s_address=model.shipping_address1;
                s_address1=model.shipping_address2;
                s_city=model.s_city;
                s_state=model.s_state;
                s_pincode=model.s_pincode;


                invoicedb.deleteaddproductitems();
                Itemadapter = new additemadapter(invoicedb.getaddproductitems(),getActivity());
                mRecyclerView.setAdapter(Itemadapter);


                if(chk_shippingaddress.isChecked()){

                    edt_address.setText(model.shipping_address1);
                    edt_address1.setText(model.shipping_address2);
                    edt_city.setText(model.s_city);
                    edt_state.setText(model.s_state);
                    edt_pincode.setText(model.s_pincode);

                }
                else{

                    edt_address.setText(model.address);
                    edt_address1.setText(model.address1);
                    edt_city.setText(model.city);
                    edt_state.setText(model.state);
                    edt_pincode.setText(model.pincode);

                }



            }
        });
        chk_shippingaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()) {
                    edt_address.setText(s_address);
                    edt_address1.setText(s_address1);
                    edt_city.setText(s_city);
                    edt_state.setText(s_state);
                    edt_pincode.setText(s_pincode);
                }
                else{
                    edt_address.setText(address);
                    edt_address1.setText(address1);
                    edt_city.setText(city);
                    edt_state.setText(state);
                    edt_pincode.setText(pincode);
                }

            }
        });

        additem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String strcustomer=edt_customername.getText().toString();

                if(TextUtils.isEmpty(strcustomer)){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Please select customer to add items");
                    toast.setView(layouttoast);
                    toast.show();
                    return;

                }
                else  if (invoicedb.searchcustomername(edt_customername.getText().toString()) == false) {
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Customer not found");
                    edt_address.setText("");
                    txt_gstno.setText("");
                    toast.setView(layouttoast);

                    toast.show();

                    return;
                }
                else {
                    Additem_fragment fragment = new Additem_fragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("Igst",str_igst);
                    bundle.putString("type","IE");
                    bundle.putString("business_type",business_type);
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();
                }

            }
        });


        submit=(Button)view.findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_customername=edt_customername.getText().toString();
                str_gstno=txt_gstno.getText().toString();
                str_address=edt_address.getText().toString();
                int m=invoicedb.getaddproductitems().size();

                for(int i=0;i<m;i++){


                    int x= Integer.parseInt(invoicedb.getaddproductitems().get(i).gstrate);

                    if(x>temp){

                        temp=x;

                    }


                }




                if(TextUtils.isEmpty(str_customername)){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Customer can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    invoicedb.deleteaddproductitems();
                    Itemadapter.notifyDataSetChanged();
                    return;

                }





                else  if(invoicedb.getaddproductitemscount()==0){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Add Items can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    return;
                }


                else {
                    String str_invoice_date = edt_inv_date.getText().toString();


                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
                    dateFormat.applyPattern("dd/MM/yy");
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy:MM:dd");
                    dateFormat1.applyPattern("yyyy/MM/dd");

                    try {
                        Date date = dateFormat.parse(str_invoice_date);
                        str_dateval = dateFormat1.format(date);




                    }
                    catch(Exception e){

                    }
                    String str_rc;
                    if (chk_rc.isChecked()) {
                        str_rc = "2";
                    } else {
                        str_rc = "1";
                    }



                    str_invoiceno=prefs.getInvoiceNo();

                    double totbeforetax = 0, totaftertax = 0, cgst = 0, sgst = 0, igst = 0;

                    str_freight = edt_freight.getText().toString();
                    str_others = edt_others.getText().toString();
                    for (int i = 0; i < invoicedb.getaddproductitemscount(); i++) {
                        double TOT_BT = totbeforetax + Double.parseDouble(invoicedb.getaddproductitems().get(i).grossamount);
                        double TOT_AT = totaftertax + Double.parseDouble(invoicedb.getaddproductitems().get(i).tat);
                        double CGST = cgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).cgst);
                        double SGST = sgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).sgst);
                        double IGST = igst + Double.parseDouble(invoicedb.getaddproductitems().get(i).igst);
                        totbeforetax = TOT_BT;
                        totaftertax = TOT_AT;
                        cgst = CGST;
                        sgst = SGST;
                        igst = IGST;
                    }

                    if(business_type.equals("2")) {

                        if (TextUtils.isEmpty(str_freight)) {
                            freightbt = 0.0;
                        } else {
                            freightbt = Double.parseDouble(str_freight);
                            freight=freightbt;
                        }

                        if (TextUtils.isEmpty(str_others)) {
                            othersbt = 0.0;
                        } else {
                            othersbt = Double.parseDouble(str_freight);
                            others=freightbt;
                        }
                    }
                    else {
                        if (TextUtils.isEmpty(str_freight)) {
                            freightbt = 0.0;
                        } else {
                            freightbt = Double.parseDouble(str_freight);
                            double xy = temp / 100;
                            double freighttax = Double.parseDouble(str_freight) * xy;
                            Double strfreightval = Double.parseDouble(str_freight) + freighttax;


                            if (str_igst.equals("N")) {

                                double CGST = cgst + Double.parseDouble(String.valueOf(freighttax / 2));
                                double SGST = sgst + Double.parseDouble(String.valueOf(freighttax / 2));
                                cgst = CGST;
                                sgst = SGST;
                                freight = freightbt + Double.parseDouble(String.valueOf(freighttax / 2)) + Double.parseDouble(String.valueOf(freighttax / 2));
                            } else {
                                double IGST = igst + Double.parseDouble(String.valueOf(freighttax));
                                igst = IGST;

                                freight = freightbt + Double.parseDouble(String.valueOf(freighttax));

                            }


                        }
                        if (TextUtils.isEmpty(str_others)) {
                            othersbt = 0.0;
                        } else {
                            othersbt = Double.parseDouble(str_others);
                            double xy = temp / 100;
                            double otherstax = Double.parseDouble(str_others) * xy;
                            Double strothersval = Double.parseDouble(str_others) + otherstax;

                            if (str_igst.equals("N")) {

                                double CGST = cgst + Double.parseDouble(String.valueOf(otherstax / 2));
                                double SGST = sgst + Double.parseDouble(String.valueOf(otherstax / 2));
                                cgst = CGST;
                                sgst = SGST;
                                others = othersbt + Double.parseDouble(String.valueOf(otherstax / 2)) + Double.parseDouble(String.valueOf(otherstax / 2));
                            } else {
                                double IGST = igst + Double.parseDouble(String.valueOf(otherstax));
                                igst = IGST;

                                others = othersbt + Double.parseDouble(String.valueOf(otherstax));

                            }
                        }
                    }
                    total_before_tax=String.valueOf(Math.round((totbeforetax+freightbt+othersbt)*100.0)/100.0);
                    total_after_tax=String.valueOf(Math.round((totaftertax+freight+others)*100.0)/100.0);
                    str_cgst=String.valueOf(Math.round(cgst*100.0)/100.0);
                    str_sgst=String.valueOf(Math.round(sgst*100.0)/100.0);
                    str_igstval=String.valueOf(Math.round(igst*100.0)/100.0);
                    invoice_customer_model invoice_customer_model=new invoice_customer_model
                            (Str_customer_name,gst_no,address,address1,phone_no,mail_id,city,state,pincode,state_pos,str_igst);
                    Invoice_details invoice=new Invoice_details(sales,str_invoice_date,str_transportmode,str_rc,str_invno,str_invno_ps,str_dateval,"B");
                    Invoice_calc invoiceCalc=new Invoice_calc(str_freight,str_others,str_cgst,str_sgst,str_igstval,total_before_tax,total_after_tax);
                    Intent myIntent = new Intent(getActivity(),ImagetoPdf.class);
                    myIntent.putExtra("invoice",invoice);
                    myIntent.putExtra("invoice_calc",invoiceCalc);
                    myIntent.putExtra("customer",invoice_customer_model);
                    myIntent.putExtra("insertorupdate","U");
                    myIntent.putExtra("invoice_id",str_getinvoiceid);
                    getActivity().startActivity(myIntent);
                }
            }
        });


        btn_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int m=invoicedb.getaddproductitems().size();

                for(int i=0;i<m;i++){
                    int x= Integer.parseInt(invoicedb.getaddproductitems().get(i).gstrate);
                    if(x>temp){
                        temp=x;
                    }
                }


                String str_freight;
                String str_others;
                String total_before_tax;
                String total_after_tax;
                String str_cgst, str_sgst, str_igstval;



                double totbeforetax = 0, totaftertax = 0, cgst = 0, sgst = 0, igst = 0;

                str_freight = edt_freight.getText().toString();
                str_others = edt_others.getText().toString();
                for (int i = 0; i < invoicedb.getaddproductitemscount(); i++) {
                    double TOT_BT = totbeforetax + Double.parseDouble(invoicedb.getaddproductitems().get(i).grossamount);
                    double TOT_AT = totaftertax + Double.parseDouble(invoicedb.getaddproductitems().get(i).tat);
                    double CGST = cgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).cgst);
                    double SGST = sgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).sgst);
                    double IGST = igst + Double.parseDouble(invoicedb.getaddproductitems().get(i).igst);
                    totbeforetax = TOT_BT;
                    totaftertax = TOT_AT;
                    cgst = CGST;
                    sgst = SGST;
                    igst = IGST;
                }
                if (TextUtils.isEmpty(str_freight)) {
                    freightbt=0.0;
                } else {
                    freightbt=Double.parseDouble(str_freight);
                    double xy= temp/100;
                    double freighttax= Double.parseDouble(str_freight)*xy;



                    if(str_igst.equals("N")){

                        double CGST = cgst + Double.parseDouble(String.valueOf(freighttax/2));
                        double SGST = sgst + Double.parseDouble(String.valueOf(freighttax/2));
                        cgst=CGST;
                        sgst=SGST;
                        freight=freightbt+Double.parseDouble(String.valueOf(freighttax/2))+Double.parseDouble(String.valueOf(freighttax/2));
                    }else{
                        double IGST = igst + Double.parseDouble(String.valueOf(freighttax));
                        igst=IGST;

                        freight=freightbt+Double.parseDouble(String.valueOf(freighttax));

                    }



                }
                if (TextUtils.isEmpty(str_others)){
                    othersbt=0.0;
                }
                else{
                    othersbt=Double.parseDouble(str_others);
                    double xy=temp/100;
                    double otherstax=Double.parseDouble(str_others)*xy;


                    if(str_igst.equals("N")){

                        double CGST = cgst + Double.parseDouble(String.valueOf(otherstax/2));
                        double SGST = sgst + Double.parseDouble(String.valueOf(otherstax/2));
                        cgst=CGST;
                        sgst=SGST;
                        others=othersbt+Double.parseDouble(String.valueOf(otherstax/2))+Double.parseDouble(String.valueOf(otherstax/2));
                    }else{
                        double IGST = igst + Double.parseDouble(String.valueOf(otherstax));
                        igst=IGST;

                        others=othersbt+Double.parseDouble(String.valueOf(otherstax));

                    }
                }


                if(business_type.equals("2")){
                    total_before_tax = String.valueOf(Math.round((totbeforetax + freightbt + othersbt) * 100.0) / 100.0);

                    tv_totalaftertax.setText(total_before_tax);



                }

                else {

                    total_before_tax = String.valueOf(Math.round((totbeforetax + freightbt + othersbt) * 100.0) / 100.0);
                    total_after_tax = String.valueOf(Math.round((totaftertax + freight + others) * 100.0) / 100.0);


                    tv_totalaftertax.setText(total_after_tax);
                    tv_totalbeforetax.setText(total_before_tax);
                    if (str_igst.equals("N")) {
                        double taxval = cgst + sgst;
                        str_cgst = String.valueOf(Math.round(taxval * 100.0) / 100.0);
                        tv_tax.setText(str_cgst);
                    } else {
                        str_igstval = String.valueOf(Math.round(igst * 100.0) / 100.0);
                        tv_tax.setText(str_igstval);

                    }

                }

            }
        });




        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }






    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.popup_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }




  public  void getinvoicedetails(final View view){

        url = Constants.GET_INVOICE_DETAILS;
        requestQueue = Volley.newRequestQueue(view.getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Getting invoice details...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String m=response.toString();
                            String x=     removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                            if (statuscode.equals("200")) {

                               JSONArray invoice_details = jsonRequest.getJSONArray("invoice_details");
                                JSONArray invoice_item= jsonRequest.getJSONArray("invoice_items");
                                for (int iStatusList = 0; iStatusList < invoice_details.length(); iStatusList++) {
                                    JSONObject invoice = invoice_details.getJSONObject(iStatusList);
                                    str_invno_ps=invoice.getString("p_invoice_no_s");
                                    str_invno=invoice.getString("invoice_no");
                                    business_type=invoice.getString("business_type");


                                    if(business_type.equals("2")){
                                        txt_totaftertax.setText("Total:");
                                        txt_totbeforetax.setVisibility(View.GONE);
                                        txt_tax.setVisibility(View.GONE);
                                        tv_tax.setVisibility(View.GONE);
                                        tv_totalbeforetax.setVisibility(View.GONE);
                                    }




                                    tv_invoiceno.setText(str_invno_ps);

                                    String strinvdate=invoice.getString("invoice_date");

                                    String[] splited = strinvdate.split("\\s+");

                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
                                    dateFormat.applyPattern("dd/MM/yy");
                                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");


                                    try {
                                        Date date = dateFormat1.parse(splited[0]);
                                        String parseddate = dateFormat.format(date);
                                        edt_inv_date.setText(parseddate);
                                    }
                                   catch(Exception e){
                                    }
                                    if(invoice.getString("sales_type").equals("1")){
                                       rb_cash.setChecked(true);
                                    }
                                    else{
                                        rb_credit.setChecked(true);

                                    }
                                    spn_transport.setSelection(Integer.parseInt(invoice.getString("transport_mode"))-1);
                                    edt_customername.setText(invoice.getString("customer_name"));
                                    txt_gstno.setText(invoice.getString("gst_no"));
                                    edt_address.setText(invoice.getString("address"));
                                    edt_address1.setText(invoice.getString("address1"));
                                    edt_city.setText(invoice.getString("city"));
                                    edt_state.setText(invoice.getString("state"));
                                    edt_pincode.setText(invoice.getString("pincode"));
                                    edt_freight.setText(invoice.getString("freight"));
                                    edt_others.setText(invoice.getString("other_charges"));
                                    Str_customer_name=invoice.getString("customer_name");
                                    gst_no=invoice.getString("gst_no");
                                    address=invoice.getString("address");
                                    phone_no=invoice.getString("phone_no");
                                    mail_id=invoice.optString("mail_id");
                                    address1=invoice.optString("address1");
                                    city=invoice.getString("city");
                                    state=invoice.getString("state");
                                    pincode=invoice.getString("pincode");
                                    state_pos=invoice.getString("state_pos");
                                     str_getinvoiceid=invoice.getString("invoice_id");
                                    str_igst=invoice.getString("igst");


                                }

                               for (int iStatusList = 0; iStatusList < invoice_item.length(); iStatusList++) {
                                    JSONObject invoiceitem = invoice_item.getJSONObject(iStatusList);
                                       addproductHashMap = new ArrayList<HashMap<String, String>>();
                                       HashMap<String, String> userhashmap = new HashMap<String, String>();
                                       userhashmap.put("product_code", invoiceitem.getString("product_code"));
                                      userhashmap.put("prod_qty", invoiceitem.getString("quantity"));
                                        userhashmap.put("prod_rate", invoiceitem.getString("amount"));
                                       userhashmap.put("prod_disc", invoiceitem.getString("discount"));
                                      userhashmap.put("prod_gst_rate", invoiceitem.getString("gst_rate"));
                                        userhashmap.put("product_desc", invoiceitem.getString("product_desc"));
                                        userhashmap.put("product_unit", invoiceitem.getString("unit"));
                                        userhashmap.put("Hsn", invoiceitem.getString("hsn_code"));
                                        userhashmap.put("cess", invoiceitem.getString("cess"));
                                        userhashmap.put("total_amount", invoiceitem.getString("total_before_tax"));
                                        userhashmap.put("discount_type", invoiceitem.getString("discount_type"));
                                        userhashmap.put("cgst", invoiceitem.getString("cgst"));
                                        userhashmap.put("sgst", invoiceitem.getString("sgst"));
                                        userhashmap.put("igst", invoiceitem.getString("igst"));
                                        userhashmap.put("cgst_percent", invoiceitem.getString("cgst_percent"));
                                        userhashmap.put("sgst_percent", invoiceitem.getString("sgst_percent"));
                                        userhashmap.put("igst_percent", invoiceitem.getString("igst_percent"));
                                        userhashmap.put("TAT",invoiceitem.getString("tat"));
                                        addproductHashMap.add(userhashmap);
                                   invoicedb.addProductitems(addproductHashMap);
                                }


                                Itemadapter = new additemadapter(invoicedb.getaddproductitems(),getActivity());
                                mRecyclerView.setAdapter(Itemadapter);
                                progressDialog.dismiss();

                            }

                            else   if (statuscode.equals("400")){
                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(jsonRequest.getString("status"));
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();

                            }


                        } catch (Exception e) {
                           //* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*//*
                            progressDialog.dismiss();


                        }
                    }
                },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(view.getContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layouttoast);
                        progressDialog.dismiss();
                        toast.show();


                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",prefs.getUserId());
                params.put("invoice_id",str_getinvoiceid);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


}

