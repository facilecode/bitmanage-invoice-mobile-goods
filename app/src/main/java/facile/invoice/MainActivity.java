package facile.invoice;

/**
 * Created by pradeep on 12/2/18.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import facile.Customer.Item_list_Main;
import facile.Purchase.Purchase_main_fragment;
import facile.Reports.Reports_main_fragment;
import facile.Settings.Settings_fragment;
import facile.localdb.Invoicedatabase;
import facile.util.prefManager;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener{
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    TextView toasttext;
    View layout;
    public static    String flag="1";
    int fragmentcurrent;
    TextView toolbartitle;
    Invoicedatabase invoicedb;

    prefManager prefs;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        prefs=new prefManager(this);
        invoicedb=new Invoicedatabase(this);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbartitle=(TextView)findViewById(R.id.toolbartitle);
       /*         mToolbar.setTitleTextColor((R.color.white));*/
        setSupportActionBar(mToolbar);
       getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        displayView(0);
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button

                    flag="0";

                    mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            onBackPressed();



                        }
                    });
                } else {

                 flag="1";

                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                    drawerFragment = (FragmentDrawer)
                            getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
                    final DrawerLayout drawerLayout=(DrawerLayout) findViewById(R.id.drawer_layout);
                    drawerFragment.setUp(R.id.fragment_navigation_drawer,drawerLayout , mToolbar);

                    mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                         /*
                          displayView(0);*/

                            drawerLayout.openDrawer(GravityCompat.START);



                        }
                    });
                }
            }
        });
    }



    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;

        switch (position) {
            case 0:

               fragment = new Sales_main_fragment();

                toolbartitle.setText(R.string.nav_item_billng);

                break;

            case 1:
                fragment = new Purchase_main_fragment();

                toolbartitle.setText(R.string.nav_item_purchase);

                break;


            case 2:
                fragment = new Item_list_Main();

                break;

            case 3:
                fragment = new Reports_main_fragment();


                break;

            case 4:
                fragment = new Settings_fragment();
              /*  title = getString(R.string.title_categories);*/
                toolbartitle.setText(R.string.title_settings);

                break;







            default:
                break;
        }

        if (fragment != null) {
            invoicedb.deleteaddproductitems();
            fragmentcurrent=position;

            FragmentManager fragmentManager = getSupportFragmentManager();

            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);


             fragmentTransaction.commit();
            // set the toolbar title
           /* getSupportActionBar().setTitle(title);*/




        }


    }

    @Override
    protected void onResume() {
        super.onResume();


      /*  // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());*/
    }

    @Override
    protected void onPause() {
      /*  LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);*/
        super.onPause();
    }

    public void setooltitle(String title) {
        toolbartitle.setText(title);
    }

  @Override
    public void onBackPressed() {

       if(flag.equals("0")){
          super.onBackPressed();

           if(fragmentcurrent==1){


               invoicedb.deleteaaddshippingaddress();

           }


      }

       else
           if (flag.equals("1")) {
               if(fragmentcurrent==3){


                   invoicedb.deleteinvoicelist();

               }
           if (fragmentcurrent == 0) {
               invoicedb.deleteaddproductitems();
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                super.onBackPressed();
            } else {
                 fragmentcurrent = 0;
               getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentManager fragmentManager = getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, new Sales_main_fragment());


                 fragmentTransaction.commit();
              //  getinvoicenodate("B");

            }
        }


  }




}






