package facile.Settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Model.Bank_details_model;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 15/4/18.
 */

public class Bank_details extends AppCompatActivity {

    EditText edt_ac_no,branch,bank_name,ifsc;
    prefManager prefs;
    Button btn_add;
    ImageView iv_edit;
    String str_acno,str_branch,str_bankname,str_ifsc,str_insertorupdate="I",str_bankid;
    ArrayList<HashMap<String, String>> bankdetailHashMap;

    String url;
    RequestQueue requestQueue;
    TextView toasttext;
    View layout;
    ProgressDialog progressDialog;
    public static final String UTF8_BOM = "\uFEFF";
    Context con;
    Invoicedatabase invoicedb;
    LinearLayout ll_bankmain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_details);
        con=this;
        prefs=new prefManager(con);
        invoicedb=new Invoicedatabase(con);
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        layout = li.inflate(R.layout.customtoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layout.findViewById(R.id.custom_toast_message);
        ll_bankmain=(LinearLayout)findViewById(R.id.ll_bankmain);
        edt_ac_no=(EditText)findViewById(R.id.edt_bankacno);
        branch=(EditText)findViewById(R.id.edt_branch);
        bank_name=(EditText)findViewById(R.id.edt_bankname);
        ifsc=(EditText)findViewById(R.id.edt_ifsc);
        iv_edit=(ImageView)findViewById(R.id.iv_edit);
        btn_add=(Button)findViewById(R.id.btn_save);
        ArrayList<Bank_details_model> items= invoicedb.getbankdetails();

        if(items.size()>0) {

           ll_bankmain.setAlpha(0.5f);
            str_bankid=items.get(0).id;
            edt_ac_no.setText(items.get(0).ac_no);
            branch.setText(items.get(0).branch);
            bank_name.setText(items.get(0).bank_name);
            ifsc.setText(items.get(0).ifsc);

            edt_ac_no.setEnabled(false);
            branch.setEnabled(false);
            bank_name.setEnabled(false);
            ifsc.setEnabled(false);

            str_insertorupdate="U";

        }
        else {
            iv_edit.setVisibility(View.GONE);
        }


        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_bankmain.setAlpha(1);
                edt_ac_no.setEnabled(true);
                branch.setEnabled(true);
                bank_name.setEnabled(true);
                ifsc.setEnabled(true);

            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_acno=edt_ac_no.getText().toString();
                str_branch=branch.getText().toString();
                str_bankname=bank_name.getText().toString();
                str_ifsc=ifsc.getText().toString();

                if(TextUtils.isEmpty(str_acno))
                {

                    edt_ac_no.requestFocus();
                    edt_ac_no.setError("Please enter your AC No");
                    return;
                }

                else  if(TextUtils.isEmpty(str_branch))
                {
                    branch.requestFocus();
                    branch.setError("Please enter your branch");

                    return;
                }


                else  if(TextUtils.isEmpty(str_bankname))
                {
                    branch.requestFocus();
                    branch.setError("Please enter your bank name");

                    return;
                }
                else  if(TextUtils.isEmpty(str_ifsc))
                {
                    branch.requestFocus();
                    branch.setError("Please enter your ifsc");

                    return;
                }

                else{
                    url = Constants.BANK_DETAILS;
                    requestQueue = Volley.newRequestQueue(getApplicationContext());
                    progressDialog = new ProgressDialog(con, R.style.MyAlertDialogStyle);
                    progressDialog.setMessage("Adding bank details..");
                    progressDialog.show();

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        String m=response.toString();
                                        String x=         removeUTF8BOM(m);
                                        JSONObject jsonRequest = new JSONObject(x);
                                        String statuscode = jsonRequest.getString("status_code");
                                        if (statuscode.equals("200")) {



                                            String id = jsonRequest.getString("id");

                                            bankdetailHashMap = new ArrayList<HashMap<String,String>>();

                                            HashMap<String, String> userhashmap = new HashMap<String, String>();
                                            userhashmap.put("id",id);
                                            userhashmap.put("ac_no", str_acno);
                                            userhashmap.put("branch",  str_branch);
                                            userhashmap.put("bank_name", str_bankname);
                                            userhashmap.put("ifsc",str_ifsc);
                                            bankdetailHashMap.add(userhashmap);
                                            if(str_insertorupdate.equals("I")){
                                                invoicedb.insertbankdetails(bankdetailHashMap);
                                                str_insertorupdate="U";
                                                str_bankid=id;


                                            }
                                            else{
                                                invoicedb.updatebankdetails(bankdetailHashMap);
                                            }
                                            Toast toast = new Toast(getApplicationContext());
                                            toast.setDuration(Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                            toasttext.setText(jsonRequest.getString("status"));
                                            toast.setView(layout);
                                            progressDialog.dismiss();
                                            toast.show();
                                        }
                                        if (statuscode.equals("400")) {
                                            Toast toast = new Toast(getApplicationContext());
                                            toast.setDuration(Toast.LENGTH_SHORT);
                                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                            toasttext.setText(jsonRequest.getString("status"));
                                            toast.setView(layout);
                                            progressDialog.dismiss();
                                            toast.show();


                                        }

                                    } catch (Exception e) {
                                        progressDialog.dismiss();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    errorhandler e=new errorhandler();
                                    String msg=e.errorhandler(error);
                                    Toast toast = new Toast(getApplicationContext());
                                    toast.setDuration(Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                    toasttext.setText(msg);
                                    toast.setView(layout);
                                    progressDialog.dismiss();
                                    toast.show();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id",prefs.getUserId());
                            params.put("ac_no",str_acno);
                            params.put("bank_name",str_bankname);
                            params.put("branch",str_branch);
                            params.put("ifsc",str_ifsc);
                            params.put("insertorupdate",str_insertorupdate);
                            if(str_insertorupdate.equals("U")){
                                params.put("updateid", str_bankid);

                            }
                            else{
                                params.put("updateid", "");
                            }

                            return params;
                        }
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);
                }
            }
        });

    }

    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

}


