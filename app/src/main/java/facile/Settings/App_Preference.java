package facile.Settings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;

import facile.Adapter.ServicetypeArrayAdapter;
import facile.Adapter.StatelistArrayAdapter;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.prefManager;

/**
 * Created by pradeep on 5/4/18.
 */

public class App_Preference  extends AppCompatActivity {
    RadioGroup radioGroup;
    prefManager prefs;
    Switch switch_rc;
    Spinner spn_state,spn_servicetype;
    Invoicedatabase invoicedatabase;
    RadioButton rb_cash,rb_credit;

    String first="0";

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.apppreference);
        prefs=new prefManager(this);
        radioGroup=(RadioGroup)findViewById(R.id.radioGroup);
        switch_rc=(Switch)findViewById(R.id.switch_rc) ;
        spn_state=(Spinner)findViewById(R.id.spn_state);
        spn_servicetype=(Spinner)findViewById(R.id.spn_serviceype);
        rb_cash=(RadioButton)findViewById(R.id.rb_cash);
        rb_credit=(RadioButton)findViewById(R.id.rb_credit);

        invoicedatabase=new Invoicedatabase(this);
        final StatelistArrayAdapter adapter = new StatelistArrayAdapter(this,
               R.layout.state_list_row, invoicedatabase.getstates());
         spn_state.setAdapter(adapter);
       final ServicetypeArrayAdapter serviceadapter = new ServicetypeArrayAdapter(this,
               R.layout.dealer_list_row, invoicedatabase.getservicetype());
       spn_servicetype.setAdapter(serviceadapter);

       spn_state.setSelection(Integer.parseInt(prefs.getConsumerState())-1);
       spn_servicetype.setSelection(Integer.parseInt(prefs.getServiceType())-1);

      /* Toast.makeText(getApplicationContext(),prefs.getConsumerState(),Toast.LENGTH_LONG).show();*/


       if(prefs.getSales().equals("1")){
           rb_cash.setChecked(true);

       }
       else{
           rb_credit.setChecked(true);

       }
       if(prefs.getReverseCharge().equals("YES")){
           switch_rc.setChecked(true);

       }
       else{
           switch_rc.setChecked(false);

       }


       radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId) {
                    case R.id.rb_cash:
                        // switch to fragment 1
                        prefs.setSales("1");
                        break;
                    case R.id.rb_credit:
                        // Fragment 2
                        prefs.setSales("2");
                        break;
                }
            }
        });

        switch_rc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    prefs.setReverseCharge("YES");
                } else {
                    // The toggle is disabled
                    prefs.setReverseCharge("NO");
                }
            }
        });



       spn_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

               /* str_state=((TextView) v.findViewById(R.id.tv_statecode)).getText().toString();*/
               if(first.equals("0")){
                   first="1";

               }
               else {

                   prefs.setConsumerState(String.valueOf(position + 1));
               }


           }

           @Override
           public void onNothingSelected(AdapterView<?> parentView) {
               // your code here
           }

       });
       spn_servicetype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {


               if(first.equals("0")){
                   first="1";

               }
               else {

                   prefs.setServiceType(String.valueOf(position + 1));
               }

           }

           @Override
           public void onNothingSelected(AdapterView<?> parentView) {
               // your code here
           }

       });



   }


}
