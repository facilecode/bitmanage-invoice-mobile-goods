package facile.Settings;

import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.DealernatureArrayAdapter;
import facile.Adapter.DealertypeArrayAdapter;
import facile.Adapter.StatelistArrayAdapter;
import facile.Model.User_detail;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;
/**
 * Created by pradeep on 14/2/18.
 */

public class profile  extends AppCompatActivity {
    Spinner spn_state,spn_dealertype,spn_dealernature;
    EditText edt_gstno,edt_address,edt_address1,edt_city,edt_pincode,edt_phno,
            edt_email,edt_bankacno,edt_bankname,edt_bankbranch,edt_ifsc,edt_tradename,edt_invoicestartno,edt_prefix,
    edt_suffix,edt_fiscalyear,edt_termsofuse;
    String str_gstno,str_address,str_address1,str_city,str_pincode,str_phno,str_email,
            str_bankacno,str_bankname,str_bankbranch,str_bankifsc,str_state,str_dealertype,str_dealernature,str_tradename,str_statepos
            ,str_dealertypepos,str_dealernaturepos,str_invoicestartno,str_prefix,str_suffix,str_termsofuse;
    Invoicedatabase invoicedb;
    public static final String UTF8_BOM = "\uFEFF";
    ArrayList<HashMap<String, String>> userdetailHashMap;
    String url;
    RequestQueue requestQueue;
    TextView toasttext;
    View layout;
    Button btn_save;
    ProgressDialog progressDialog;
    prefManager prefs;
    ImageView edit_profile;
    LinearLayout ll_userdetails;
    ImageView companylogoo;
    private int PICK_IMAGE_REQUEST = 1;
    Bitmap bitmap;
    Uri originalUri;

    String encodedImage="";
    File imgfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_details);
        prefs=new prefManager(this);
        invoicedb=new Invoicedatabase(this);
        spn_state=(Spinner)findViewById(R.id.spn_state);
        spn_dealertype=(Spinner)findViewById(R.id.spn_dealertype);
        spn_dealernature=(Spinner)findViewById(R.id.spn_dealernature);
        edt_city=(EditText)findViewById(R.id.edt_city);
        edt_address=(EditText)findViewById(R.id.edt_address);
        edt_address1=(EditText)findViewById(R.id.edt_address1);
        edt_pincode=(EditText)findViewById(R.id.edt_pincode);
        edt_phno=(EditText)findViewById(R.id.edt_userphoneno);
        edt_city=(EditText)findViewById(R.id.edt_city);
        edt_email=(EditText)findViewById(R.id.edt_usermailid);
        edt_gstno=(EditText)findViewById(R.id.edt_gstno);
        edt_tradename=(EditText)findViewById(R.id.edt_tradename);
        edt_termsofuse=(EditText)findViewById(R.id.edt_invoicetermsofuse);
        companylogoo=(ImageView)findViewById(R.id.iv_companylogoo);
      /*  edt_bankacno=(EditText)findViewById(R.id.edt_bankacno);
        edt_bankname=(EditText)findViewById(R.id.edt_bankname);
        edt_bankbranch=(EditText)findViewById(R.id.edt_branch);
        edt_ifsc=(EditText)findViewById(R.id.edt_ifsc);*/
        edt_prefix=(EditText)findViewById(R.id.edt_prefix);
        edt_suffix=(EditText)findViewById(R.id.edt_suffix);
        edt_fiscalyear=(EditText)findViewById(R.id.edt_fiscalyear);
        btn_save=(Button)findViewById(R.id.btn_save);
        edit_profile=(ImageView)findViewById(R.id.iv_edit);
        edt_invoicestartno=(EditText)findViewById(R.id.edt_invoiceno);
        ll_userdetails=(LinearLayout)findViewById(R.id.ll_userdetailsmain);
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        layout = li.inflate(R.layout.customtoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layout.findViewById(R.id.custom_toast_message);
        final StatelistArrayAdapter adapter = new StatelistArrayAdapter(this,
                R.layout.state_list_row, invoicedb.getstates());
        spn_state.setAdapter(adapter);
        final DealertypeArrayAdapter adapter1 = new DealertypeArrayAdapter(this,
                R.layout.dealer_list_row, invoicedb.getdealertype());
        spn_dealertype.setAdapter(adapter1);
        final DealernatureArrayAdapter adapter2 = new DealernatureArrayAdapter(this,
                R.layout.dealer_list_row, invoicedb.getdealernature());
        spn_dealernature.setAdapter(adapter2);
        if(prefs.getLogInType().equals("0")){
            editenabled(true);
        }
        else{
            ArrayList<User_detail> items= invoicedb.getuserdetails();
            edt_tradename.setText(items.get(0).user_trade_name);
            edt_gstno.setText(items.get(0).gst_no);
            edt_address.setText(items.get(0).address);
            edt_address1.setText(items.get(0).address1);
            edt_city.setText(items.get(0).city);
            edt_pincode.setText(items.get(0).pincode);
            edt_phno.setText(items.get(0).phone_no);
            edt_email.setText(items.get(0).mail_id);
            edt_termsofuse.setText(items.get(0).terms_of_use);
           /* edt_bankacno.setText(items.get(0).bank_ac_no);
            edt_bankname.setText(items.get(0).bank_name);
            edt_bankbranch.setText(items.get(0).bank_branch);
            edt_ifsc.setText(items.get(0).bank_ifsc);*/
            spn_state.setSelection(Integer.parseInt(items.get(0).state_pos)-1);
            spn_dealertype.setSelection(Integer.parseInt(items.get(0).dealer_type)-1);
            spn_dealernature.setSelection(Integer.parseInt(items.get(0).dealer_nature)-1);
            edt_invoicestartno.setText(items.get(0).invoice_start_no);
            edt_prefix.setText(items.get(0).prefix);
            edt_suffix.setText(items.get(0).suffix);
            edt_invoicestartno.setFocusable(false);
            edt_fiscalyear.setFocusable(false);




    try {
        imgfile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/BizManager/profilepic.png");
        if (imgfile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgfile.getAbsolutePath());
            companylogoo.setImageBitmap(myBitmap);

        }
    } catch (Exception e) {



}
            editenabled(false);
            ll_userdetails.setAlpha(0.5f);
        }
edit_profile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        ll_userdetails.setAlpha(1);
        editenabled(true);

    }
});

        spn_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                str_state=((TextView) v.findViewById(R.id.tv_statename)).getText().toString();
                str_statepos=String.valueOf(position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spn_dealertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                str_dealertype=((TextView) v.findViewById(R.id.tv_dealerid)).getText().toString();
                str_dealertypepos=String.valueOf(position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        spn_dealernature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

                str_dealernature=((TextView) v.findViewById(R.id.tv_dealerid)).getText().toString();
                str_dealernaturepos=String.valueOf(position+1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });




        companylogoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();

                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });


                btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_gstno=edt_gstno.getText().toString();
                str_address=edt_address.getText().toString();
                str_address1=edt_address1.getText().toString();
               /* str_bankacno=edt_bankacno.getText().toString();
                str_bankbranch=edt_bankbranch.getText().toString();
                str_bankname=edt_bankname.getText().toString();
                str_bankifsc=edt_ifsc.getText().toString();*/
                str_city=edt_city.getText().toString();
                str_pincode=edt_pincode.getText().toString();
                str_phno=edt_phno.getText().toString();
                str_email=edt_email.getText().toString();
                str_tradename=edt_tradename.getText().toString();
                str_invoicestartno=edt_invoicestartno.getText().toString();
                str_prefix=edt_prefix.getText().toString();
                str_suffix=edt_suffix.getText().toString();
                str_termsofuse=edt_termsofuse.getText().toString();

                if(TextUtils.isEmpty(str_tradename))
                {
                     edt_tradename.requestFocus();
                    edt_tradename.setError("Please enter your Trade name");

                    return;
                }

              else  if(TextUtils.isEmpty(str_gstno))
                {
                    edt_gstno.requestFocus();
                    edt_gstno.setError("Please enter your Gst Number");

                    return;
                }
            else    if(TextUtils.isEmpty(str_address))
                {
                    edt_address.requestFocus();
                    edt_address.setError("Please enter your Address");

                    return;
                }
           else     if(TextUtils.isEmpty(str_city))
                {
                    edt_city.requestFocus();
                    edt_city.setError("Please enter your city");

                    return;
                }
             else   if(TextUtils.isEmpty(str_pincode))
                {
                    edt_pincode.requestFocus();
                    edt_pincode.setError("Please enter your pincode");

                    return;
                }
                else if(TextUtils.isEmpty(str_phno))
                {
                    edt_phno.requestFocus();
                    edt_phno.setError("Please enter your phone number");

                    return;
                }

                else  if(str_email.length()>0) {

                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(str_email).matches()) {
                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText("Enter valid email");
                        toast.setView(layout);

                        toast.show();
                        return;
                    }
                }


                if(TextUtils.isEmpty(str_invoicestartno))
                    {
                        str_invoicestartno="00001";
                    }


                if (originalUri==null) {


                }

else{
                    encodedImage = compressImage(originalUri.toString());
                }


                    setprofile();


            }
        });



    }

    public void setprofile(){

        url = Constants.USER_DETAILS;
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        progressDialog = new ProgressDialog(this, R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Saving User details..");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            String m=response.toString();
                            String x=         removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                            if (statuscode.equals("200")) {
                               userdetailHashMap = new ArrayList<HashMap<String,String>>();
                                HashMap<String, String> userhashmap = new HashMap<String, String>();
                                userhashmap.put("address", str_address);
                                userhashmap.put("address1", str_address1);
                                userhashmap.put("phone_no",str_phno);
                                userhashmap.put("mail_id", str_email);
                                userhashmap.put("city", str_city);
                                userhashmap.put("state", str_state);
                                prefs.setState(str_statepos);
                                userhashmap.put("pincode",str_pincode);
                                userhashmap.put("dealer_type", str_dealertypepos);
                                userhashmap.put("dealer_nature", str_dealernaturepos);
                                prefs.setBusinessNature(str_dealernaturepos);
                                prefs.setBusinessType(str_dealertypepos);
                                userhashmap.put("gst_no", str_gstno);

                                String logo=jsonRequest.getString("logo");
                                if(jsonRequest.isNull("logo")){
                                    prefs.setLogo("");
                                }else {
                                    prefs.setLogo(logo);
                                }
                               /* userhashmap.put("bank_ac_no", str_bankacno);
                                userhashmap.put("bank_name", str_bankname);
                                userhashmap.put("bank_branch", str_bankbranch);
                                userhashmap.put("bank_ifsc", str_bankifsc);*/
                                userhashmap.put("user_id", prefs.getUserId());
                                userhashmap.put("user_trade_name", str_tradename);
                                prefs.setCompanyName(str_tradename);


                                userhashmap.put("insertorupdate", str_tradename);
                                userhashmap.put("spn_state_position", str_statepos);
                                userhashmap.put("invoice_start_no", str_invoicestartno);
                                userhashmap.put("invoice_no_prefix", str_prefix);
                                userhashmap.put("invoice_no_suffix", str_suffix);
                                userhashmap.put("terms_of_use", str_termsofuse);
                                userdetailHashMap.add(userhashmap);
                               if(prefs.getLogInType().equals("0")){

                                    invoicedb.insertUserdetails(userdetailHashMap);
                               }
                              else{

                                    invoicedb.updateUserdetails(userdetailHashMap);
                                }

                           }
                            Toast toast = new Toast(getApplicationContext());
                            toast.setDuration(Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toasttext.setText("User Details saved successfully");
                            toast.setView(layout);
                            progressDialog.dismiss();
                            toast.show();
                            progressDialog.dismiss();

                            String sa=prefs.getLogo();

                            if(prefs.getLogo().isEmpty()) {

                            }
                            else{

                                new DownloadImage().execute(prefs.getLogo());
                            }

                            if(prefs.getLogInType().equals("0")){
                                prefs.setLogInType("1");
                                Intent i = new Intent(profile.this, MainActivity.class);
                                startActivity(i);
                                finish();
                           }
                        } catch (Exception e) {
                            progressDialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(getApplicationContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layout);
                        progressDialog.dismiss();
                        toast.show();


                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("companylogo",encodedImage);
                params.put("gst_no", str_gstno);
                params.put("user_id",prefs.getUserId());
                params.put("dealer_type",str_dealertypepos);
                params.put("dealer_nature",str_dealernaturepos);
                params.put("tradename",str_tradename);
                params.put("address",str_address);
                params.put("address1",str_address1);
                params.put("city",str_city);
                params.put("pincode",str_pincode);
                params.put("state",str_state);
                params.put("ph_no",str_phno);
                params.put("email_id",str_email);
             /*   params.put("bank_ac_no",str_bankacno);
                params.put("bank_name",str_bankname);
                params.put("bank_branch",str_bankbranch);
                params.put("bank_ifsc",str_bankifsc);*/
                params.put("spn_state_position",str_statepos);
                params.put("invoice_no_prefix",str_prefix);
                params.put("invoice_no_suffix",str_suffix);
                params.put("terms_of_use",str_termsofuse);
                if(prefs.getLogInType().equals("0")){
                    params.put("insertorupdate","I");
                    params.put("invoice_start_no", str_invoicestartno);

                }else{
                    params.put("insertorupdate","U");
                    params.put("invoice_start_no","01");
                }
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

public void  editenabled(boolean flag){

    if(flag==true){
        edt_tradename.setEnabled(true);
        spn_state.setEnabled(true);
        spn_dealertype.setEnabled(true);
        edt_gstno.setEnabled(true);
        edt_address.setEnabled(true);
        edt_city.setEnabled(true);
        edt_pincode.setEnabled(true);
        edt_phno.setEnabled(true);
        edt_email.setEnabled(true);
      /*  edt_bankacno.setEnabled(true);
        edt_bankname.setEnabled(true);
        edt_bankbranch.setEnabled(true);
        edt_ifsc.setEnabled(true);*/
        edt_prefix.setEnabled(true);
        edt_suffix.setEnabled(true);
    }
else{
        spn_dealertype.setEnabled(false);
        spn_state.setEnabled(false);
        edt_tradename.setEnabled(false);
        edt_gstno.setEnabled(false);
        edt_address.setEnabled(false);
        edt_city.setEnabled(false);
        edt_pincode.setEnabled(false);
        edt_phno.setEnabled(false);
        edt_email.setEnabled(false);
      /*  edt_bankacno.setEnabled(false);
        edt_bankname.setEnabled(false);
        edt_bankbranch.setEnabled(false);
        edt_ifsc.setEnabled(false);*/
        edt_prefix.setEnabled(false);
        edt_suffix.setEnabled(false);

    }
}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data); comment this unless you want to pass your result to the activity.
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                originalUri = data.getData();
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);


                companylogoo.setImageBitmap(bitmap);
            }
            catch(Exception e){

            }
        }
    }


    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG,90, baos);
        return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);

    }
    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }
    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        private String TAG = "DownloadImage";
        private Bitmap downloadImageBitmap(String sUrl) {
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new URL(sUrl).openStream();   // Download Image from URL
                bitmap = BitmapFactory.decodeStream(inputStream);       // Decode Bitmap
                inputStream.close();
            } catch (Exception e) {
                Log.d(TAG, "Exception 1, Something went wrong!");
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downloadImageBitmap(params[0]);
        }

        protected void onPostExecute(Bitmap result) {
            saveToExternalStorage( result);
        }
    }


    private String saveToExternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File mypath;

        File filepath = Environment.getExternalStorageDirectory();

        // Create a new folder in SD Card
        File dir = new File(filepath.getAbsolutePath()
                + "/BizManager/");
        dir.mkdirs();

        File dir1 = new File(filepath.getAbsolutePath()
                + "/BizManager/Invoice/");
        dir1.mkdirs();

        mypath = new File(dir, "profilepic.png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                fos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dir.getAbsolutePath();
    }



}