package facile.Purchase;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.non_closure_invoice_list_adapter;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.SimpleDividerItemDecoration;
import facile.util.errorhandler;
import facile.util.prefManager;

public class Purchase_list extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public static final String UTF8_BOM = "\uFEFF";
    ProgressDialog progressDialog;
    String url;
    prefManager prefs;
    View layouttoast;
    TextView toasttext;

    ArrayList<HashMap<String, String>> reporthashmap;
    HashMap<String, String> userhashmap;

    RequestQueue requestQueue;

    private Purchase_list.OnFragmentInteractionListener mListener;
    RecyclerView rv_invoicelist;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public   String first="0";
    Invoicedatabase invoicedb;

    public Purchase_list() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment invoice_list.
     */
    // TODO: Rename and change types and number of parameters
    public static Purchase_list newInstance(String param1, String param2) {
        Purchase_list fragment = new Purchase_list();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_invoice_list, container, false);
        prefs = new prefManager(getContext());
        invoicedb = new Invoicedatabase(getContext());
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        rv_invoicelist=(RecyclerView)view.findViewById(R.id.rv_invoicelist);
        rv_invoicelist.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        rv_invoicelist.setLayoutManager(mLayoutManager);
        rv_invoicelist.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        if(first.equals("0")) {


            getnonclosurereport(view);
            first="1";
        }else{

            invoicedb.deleteaddproductitems();
            mAdapter = new non_closure_invoice_list_adapter(getContext(),invoicedb.getinvoicedetails());
            rv_invoicelist.setAdapter(mAdapter);




        }

        mAdapter = new non_closure_invoice_list_adapter(getContext(),invoicedb.getinvoicedetails());
        rv_invoicelist.setAdapter(mAdapter);

        ((non_closure_invoice_list_adapter) mAdapter).setOnItemClickListener(new non_closure_invoice_list_adapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                //   Toast.makeText(getContext(),reporthashmap.get(position-1).get("invoiveno"),Toast.LENGTH_LONG).show();

                   invoicedb.deleteaddproductitems();
                    Purchase_edit fragment = new Purchase_edit();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("invoice_id", reporthashmap.get(position - 1).get("id"));
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();

            }

        });

        return view;
    }


    public void getnonclosurereport(final View view){
        url = Constants.NON_CLOSURE_REPORT_PURCHASE;
        requestQueue = Volley.newRequestQueue(view.getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Generating report...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String m=response.toString();
                            String x=     removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                            String   val = jsonRequest.getString("status");
                            if (statuscode.equals("200")) {
                                JSONObject json = new JSONObject(jsonRequest.toString());
                                if (json.isNull("report")) {

                                } else {
                                    JSONArray report_list = jsonRequest.getJSONArray("report");
                                    reporthashmap = new ArrayList<HashMap<String, String>>();
                                    for (int iStatusList = 0; iStatusList < report_list.length(); iStatusList++) {
                                        JSONObject report = report_list.getJSONObject(iStatusList);
                                        userhashmap = new HashMap<String, String>();
                                        userhashmap.put("id", report.getString("id"));
                                        userhashmap.put("invoiveno", report.getString("invoice_no"));
                                        userhashmap.put("customer_name", report.getString("supplier_name"));
                                        userhashmap.put("tat", report.getString("tat"));

                                        reporthashmap.add(userhashmap);
                                    }
                                    invoicedb.insertinvoicelist(reporthashmap);

                                    mAdapter = new non_closure_invoice_list_adapter(getContext(),invoicedb.getinvoicedetails());
                                    rv_invoicelist.setAdapter(mAdapter);


                                }

                                progressDialog.dismiss();


                            }
                            else   if (statuscode.equals("600")){
                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(val);
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();

                            }

                        } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                            progressDialog.dismiss();


                        }
                    }
                },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(view.getContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layouttoast);
                        progressDialog.dismiss();
                        toast.show();


                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",prefs.getUserId());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
