package facile.Purchase;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.StatelistArrayAdapter;
import facile.Model.Shipping_model;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 12/2/18.
 */

public class Supplier_creation extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String url;
    public static final String UTF8_BOM = "\uFEFF";
    public static int CODE=122;
    RequestQueue requestQueue;
    private Supplier_creation.OnFragmentInteractionListener mListener;
    ProgressDialog progressDialog;
    EditText gst1,gst2,gst3,gst4,gst5,gst6,gst7,gst8,gst9,gst10,gst11,gst12,gst13,gst14,gst15;
    TextView toasttext;
    View layouttoast;
    EditText edt_customername,edt_address,edt_address1,edt_city,edt_phoneno,edt_pincode,edt_mailid,edt_openingbal,edt_gstno;
    String str_gstno,str_suppliername,str_address,str_address1,str_city,str_pincode,str_state="",str_phno,str_statepos,
            str_mailid,str_address_s,str_address1_s,str_city_s,str_pincode_s,str_state_s,str_country_s,str_openingbal,
            str_crordr="1",str_supplier_id;
    RadioGroup radioGroup;
    RadioButton rb_plus,rb_minus;
    Button btn_add,btn_add_shipping_address;
    Spinner   spn_state;
    Invoicedatabase invoicedb;
    prefManager prefs;
    ImageView iv_edit;
    String str_insertorupdate;
    LinearLayout ll_customermain;
    ArrayList<HashMap<String, String>> supplierdetailHashMap;
    CheckBox chk_nogstno;

    private ArrayList<Shipping_model> mDataset;
    String edit="0";


    public Supplier_creation() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Supplier_creation newInstance(String param1, String param2) {
        Supplier_creation fragment = new Supplier_creation();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final  View view = inflater.inflate(R.layout.create_customer, container, false);
        invoicedb=new Invoicedatabase(getContext());
        ((MainActivity) getActivity()).setooltitle(getString(R.string.supplier));
        prefs=new prefManager(getContext());
        gst1=(EditText)view.findViewById(R.id.edt_gstno1);
        gst2=(EditText)view.findViewById(R.id.edt_gstno2);
        gst3=(EditText)view.findViewById(R.id.edt_gstno3);
        gst4=(EditText)view.findViewById(R.id.edt_gstno4);
        gst5=(EditText)view.findViewById(R.id.edt_gstno5);
        gst6=(EditText)view.findViewById(R.id.edt_gstno6);
        gst7=(EditText)view.findViewById(R.id.edt_gstno7);
        gst8=(EditText)view.findViewById(R.id.edt_gstno8);
        gst9=(EditText)view.findViewById(R.id.edt_gstno9);
        gst10=(EditText)view.findViewById(R.id.edt_gstno10) ;
        gst11=(EditText)view.findViewById(R.id.edt_gstno11) ;
        gst12=(EditText)view.findViewById(R.id.edt_gstno12) ;
        gst13=(EditText)view.findViewById(R.id.edt_gstno13) ;
        gst14=(EditText)view.findViewById(R.id.edt_gstno14) ;
        gst15=(EditText)view.findViewById(R.id.edt_gstno15) ;
        edt_gstno=(EditText)view.findViewById(R.id.edt_gstno);
        gst14.setEnabled(false);
        chk_nogstno=(CheckBox)view.findViewById(R.id.chk_nogstno);
        ll_customermain=(LinearLayout)view.findViewById(R.id.ll_customermain);
        spn_state=(Spinner)view.findViewById(R.id.spn_state);
        btn_add=(Button) view.findViewById(R.id.btn_add);
        btn_add_shipping_address=(Button)view.findViewById(R.id.btn_addshippingaddress);
        edt_customername=(EditText)view.findViewById(R.id.edt_customername);
        edt_customername.setHint("Supplier Name");
        edt_address=(EditText)view.findViewById(R.id.edt_address);
        edt_address1=(EditText)view.findViewById(R.id.edt_address1);
        edt_city=(EditText)view.findViewById(R.id.edt_city);
        edt_pincode=(EditText)view.findViewById(R.id.edt_pincode);
        edt_phoneno=(EditText)view.findViewById(R.id.edt_customerphoneno);
        edt_mailid=(EditText)view.findViewById(R.id.edt_customermailid);
        edt_openingbal=(EditText)view.findViewById(R.id.edt_openingbal);

        btn_add_shipping_address.setVisibility(View.GONE);
        radioGroup=(RadioGroup)view.findViewById(R.id.radioGroup);
        rb_plus=(RadioButton)view.findViewById(R.id.rb_plus);
        rb_minus=(RadioButton)view.findViewById(R.id.rb_minus);

        iv_edit=(ImageView)view.findViewById(R.id.iv_edit);
        rb_plus.setChecked(true);
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        final StatelistArrayAdapter adapter = new StatelistArrayAdapter(getActivity(),
                R.layout.state_list_row, invoicedb.getstates());
        spn_state.setAdapter(adapter);

        Bundle bundle = this.getArguments();
        chk_nogstno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()) {
                    gst1.setEnabled(false);
                    gst2.setEnabled(false);
                    gst3.setEnabled(false);
                    gst4.setEnabled(false);
                    gst5.setEnabled(false);
                    gst6.setEnabled(false);
                    gst7.setEnabled(false);
                    gst8.setEnabled(false);
                    gst9.setEnabled(false);
                    gst10.setEnabled(false);
                    gst11.setEnabled(false);
                    gst12.setEnabled(false);
                    gst13.setEnabled(false);
                    gst14.setEnabled(false);
                    gst15.setEnabled(false);
                    spn_state.setEnabled(true);
                    spn_state.setSelection(0);
                    gst1.setText("");
                    gst2.setText("");
                    gst3.setText("");
                    gst4.setText("");
                    gst5.setText("");
                    gst6.setText("");
                    gst7.setText("");
                    gst8.setText("");
                    gst9.setText("");
                    gst10.setText("");
                    gst11.setText("");
                    gst12.setText("");
                    gst13.setText("");
                    gst14.setText("");
                    gst15.setText("");
                }
                else{
                    gst1.setEnabled(true);
                    gst2.setEnabled(true);
                    gst3.setEnabled(true);
                    gst4.setEnabled(true);
                    gst5.setEnabled(true);
                    gst6.setEnabled(true);
                    gst7.setEnabled(true);
                    gst8.setEnabled(true);
                    gst9.setEnabled(true);
                    gst10.setEnabled(true);
                    gst11.setEnabled(true);
                    gst12.setEnabled(true);
                    gst13.setEnabled(true);
                    gst15.setEnabled(true);
                    gst14.setText("Z");
                    spn_state.setEnabled(false);
                    spn_state.setSelection(0);
                }
            }
        });
        if (bundle != null) {
            str_supplier_id=bundle.getString("SUPPLIER_ID", "");
            str_suppliername = bundle.getString("SUPPLIER_NAME", "");
            str_gstno= bundle.getString("GST_NO", "");
            str_address=bundle.getString("ADDRESS", "");
            str_address1=bundle.getString("ADDRESS1", "");
            str_state=bundle.getString("STATE", "");
            str_pincode=bundle.getString("PINCODE", "");
            str_city=bundle.getString("CITY", "");
            str_phno=bundle.getString("PHONE_NO", "");
            str_mailid=bundle.getString("MAIL_ID", "");
            str_statepos=bundle.getString("STATE_POS", "");

            str_crordr=bundle.getString("DRORCR", "");


            gst1.setVisibility(View.GONE);
            gst2.setVisibility(View.GONE);
            gst3.setVisibility(View.GONE);
            gst4.setVisibility(View.GONE);
            gst5.setVisibility(View.GONE);
            gst6.setVisibility(View.GONE);
            gst7.setVisibility(View.GONE);
            gst8.setVisibility(View.GONE);
            gst9.setVisibility(View.GONE);
            gst10.setVisibility(View.GONE);
            gst11.setVisibility(View.GONE);
            gst12.setVisibility(View.GONE);
            gst13.setVisibility(View.GONE);
            gst14.setVisibility(View.GONE);
            gst15.setVisibility(View.GONE);
            edt_gstno.setVisibility(View.VISIBLE);
            edt_customername.setText(str_suppliername);
            if(str_gstno.equals("NA")){

                chk_nogstno.setChecked(true);
                spn_state.setSelection(Integer.parseInt(str_statepos));

            }

            edt_gstno.setText(str_gstno);
            edt_address.setText(str_address);
            edt_address1.setText(str_address1);
            edt_city.setText(str_city);
            edt_pincode.setText(str_pincode);
            edt_phoneno.setText(str_phno);
            edt_mailid.setText(str_mailid);
            edt_openingbal.setText(str_openingbal);
            spn_state.setSelection(Integer.parseInt(str_statepos)-1);
            if(edit.equals("1")){
                edt_customername.setEnabled(true);
                edt_address.setEnabled(true);
                edt_address1.setEnabled(true);
                edt_city.setEnabled(true);
                edt_pincode.setEnabled(true);
                edt_phoneno.setEnabled(true);
                edt_mailid.setEnabled(true);
                btn_add_shipping_address.setEnabled(true);
                if(str_gstno.equals("NA")){
                    spn_state.setEnabled(true);
                }
                else {
                    spn_state.setEnabled(false);
                }
                edt_openingbal.setEnabled(true);
                edt_address.requestFocus();
                ll_customermain.setAlpha(1);
                edt_gstno.setAlpha(0.5f);
                btn_add.setVisibility(View.VISIBLE);
                btn_add.setText("UPDATE");
            }
            else {
                edt_customername.setEnabled(false);
                edt_gstno.setEnabled(false);
                edt_address.setEnabled(false);
                edt_address1.setEnabled(false);
                edt_city.setEnabled(false);
                edt_pincode.setEnabled(false);
                edt_phoneno.setEnabled(false);
                edt_mailid.setEnabled(false);
                spn_state.setEnabled(false);
                edt_openingbal.setEnabled(false);
                btn_add_shipping_address.setEnabled(false);
                str_insertorupdate = "U";
                ll_customermain.setAlpha(0.5f);
                btn_add.setVisibility(View.GONE);
                chk_nogstno.setVisibility(View.GONE);
            }
            if(str_crordr.equals("1")){

                rb_plus.setChecked(true);

            }
            else{
                rb_minus.setChecked(true);

            }

           /* if(edt_edtgstno.getText().toString().equals("NA")) {
                iv_edit.setVisibility(View.GONE);
            }*/
        }

        else{
            ll_customermain.setAlpha(1);
            iv_edit.setVisibility(View.GONE);
            str_insertorupdate="I";
            edt_customername.requestFocus();
            spn_state.setEnabled(false);

        }


        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit="1";
                edt_customername.setEnabled(true);
                edt_address.setEnabled(true);
                edt_address1.setEnabled(true);
                edt_city.setEnabled(true);
                edt_pincode.setEnabled(true);
                edt_phoneno.setEnabled(true);
                edt_mailid.setEnabled(true);
                btn_add_shipping_address.setEnabled(true);
                if(str_gstno.equals("NA")){
                    spn_state.setEnabled(true);
                }
                else {
                    spn_state.setEnabled(false);
                }
                edt_openingbal.setEnabled(true);
                edt_address.requestFocus();
                ll_customermain.setAlpha(1);
                edt_gstno.setAlpha(0.5f);

                btn_add.setVisibility(View.VISIBLE);
                btn_add.setText("UPDATE");


            }
        });



        spn_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {

               /* str_state=((TextView) v.findViewById(R.id.tv_statecode)).getText().toString();*/
                str_statepos=String.valueOf(position+1);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });





        gst1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst1.getText().toString().length()==1)     //size as per your requirement
                {
                    gst2.requestFocus();
                    if(s.length() != 0 && gst2.length()!=0){
                        String m=gst1.getText().toString()+gst2.getText().toString();
                        ArrayList<String> val=invoicedb.getstatebyposition(m);
                        if(val!=null) {
                            spn_state.setSelection(Integer.parseInt(val.get(0)) - 1);
                            str_state=val.get(1).toString();
                            str_statepos=String.valueOf(Integer.parseInt(val.get(0)) );
                        }

                        else{
                            Toast toast = new Toast(view.getContext());
                            toast.setDuration(Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toasttext.setText("Invalid GST Number");
                            toast.setView(layouttoast);
                            toast.show();
                            gst1.setText("");
                            gst2.setText("");
                            gst1.requestFocus();


                        }

                    }


                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });


        gst2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst2.getText().toString().length()==1)     //size as per your requirement
                {
                    gst3.requestFocus();
                    if(s.length() != 0 && gst2.length()!=0){
                        String m=gst1.getText().toString()+gst2.getText().toString();
                        ArrayList<String> val=invoicedb.getstatebyposition(m);
                        if(val!=null) {
                            spn_state.setSelection(Integer.parseInt(val.get(0)) - 1);
                            str_state=val.get(1).toString();
                            str_statepos=String.valueOf(Integer.parseInt(val.get(0)) );
                        }
                        else{


                            Toast toast = new Toast(view.getContext());
                            toast.setDuration(Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toasttext.setText("Invalid GST Number");
                            toast.setView(layouttoast);

                            toast.show();
                            gst1.setText("");
                            gst2.setText("");

                            gst1.requestFocus();

                        }

                    }





                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst2.getText().length()==1){
                        gst2.setText("");
                        gst2.setSelection(0);

                    }
                    else{
                        gst1.requestFocus();
                        gst1.setText("");
                    }

                }
                return false;
            }
        });

        gst3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst3.getText().toString().length()==1)     //size as per your requirement
                {
                    gst4.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst3.getText().length()==1){
                        gst3.setText("");
                        gst3.setSelection(0);
                    }
                    else{
                        gst2.requestFocus();
                        gst2.setText("");
                    }

                }
                return false;
            }
        });

        gst4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst4.getText().toString().length()==1)     //size as per your requirement
                {
                    gst5.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        gst4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst4.getText().length()==1){
                        gst4.setText("");
                        gst4.setSelection(0);
                    }
                    else{
                        gst3.requestFocus();
                        gst3.setText("");
                    }

                }
                return false;
            }
        });
        gst5.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst5.getText().toString().length()==1)     //size as per your requirement
                {
                    gst6.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst5.getText().length()==1){

                    }
                    else{
                        gst4.requestFocus();
                        gst4.setText("");
                    }

                }
                return false;
            }
        });

        gst6.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst6.getText().toString().length()==1)     //size as per your requirement
                {
                    gst7.requestFocus();
                    if (s.length() != 0 && gst6.length() != 0) {
                        String m = gst6.getText().toString();

                        if (m.matches(".*[PCHFTBLGJApchftblgja].*")) {

                        } else {


                            Toast toast = new Toast(view.getContext());
                            toast.setDuration(Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toasttext.setText("Invalid GST Number");
                            toast.setView(layouttoast);

                            toast.show();
                            gst6.setText("");


                            gst6.requestFocus();

                        }

                    }
                }


            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        gst6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst6.getText().length()==1){

                    }
                    else{
                        gst5.requestFocus();
                        gst5.setText("");
                    }

                }
                return false;
            }
        });

        gst7.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst7.getText().toString().length()==1)     //size as per your requirement
                {
                    gst8.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst7.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst7.getText().length()==1){

                    }
                    else{
                        gst6.requestFocus();
                        gst6.setText("");
                    }

                }
                return false;
            }
        });
        gst8.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst8.getText().toString().length()==1)     //size as per your requirement
                {
                    gst9.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst8.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst8.getText().length()==1){

                    }
                    else{
                        gst7.requestFocus();
                        gst7.setText("");
                    }

                }
                return false;
            }
        });


        gst9.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst9.getText().toString().length()==1)     //size as per your requirement
                {
                    gst10.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        gst9.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst9.getText().length()==1){

                    }
                    else{
                        gst8.requestFocus();
                        gst8.setText("");
                    }

                }
                return false;
            }
        });

        gst10.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst10.getText().toString().length()==1)     //size as per your requirement
                {
                    gst11.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst10.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst10.getText().length()==1){

                    }
                    else{
                        gst9.requestFocus();
                        gst9.setText("");
                    }

                }
                return false;
            }
        });

        gst11.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst11.getText().toString().length()==1)     //size as per your requirement
                {
                    gst12.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst11.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst11.getText().length()==1){

                    }
                    else{
                        gst10.requestFocus();
                        gst10.setText("");
                    }

                }
                return false;
            }
        });
        gst12.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst12.getText().toString().length()==1)     //size as per your requirement
                {
                    gst13.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        gst12.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst12.getText().length()==1){

                    }
                    else{
                        gst11.requestFocus();
                        gst11.setText("");
                    }

                }
                return false;
            }
        });
        gst13.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(gst13.getText().toString().length()==1)     //size as per your requirement
                {
                    gst15.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst13.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst13.getText().length()==1){

                    }
                    else{
                        gst12.requestFocus();
                        gst12.setText("");
                    }

                }
                return false;
            }
        });

        gst15.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
             /*   if(gst15.getText().toString().length()==1)     //size as per your requirement
                {
                    gst15.requestFocus();
                }*/
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
        gst15.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    if(gst15.getText().length()==1){

                    }
                    else{
                        gst13.requestFocus();
                        gst13.setText("");
                    }

                }
                return false;
            }
        });


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addcustomer(view);

            }
        });


        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }





    public  void addcustomer(final View view) {

        url = Constants.ADD_SUPPLIER;

        if (str_insertorupdate.equals("I")) {

            if (chk_nogstno.isChecked()) {

                str_gstno = "NA";

            } else {


                str_gstno = gst1.getText().toString() + gst2.getText().toString() + gst3.getText().toString() + gst4.getText().toString() + gst5.getText().toString() +
                        gst6.getText().toString() + gst7.getText().toString() + gst8.getText().toString() + gst9.getText().toString() + gst10.getText().toString()
                        + gst11.getText().toString() + gst12.getText().toString() + gst13.getText().toString() + gst14.getText().toString() + gst15.getText().toString();

            }
        }
        str_suppliername = edt_customername.getText().toString();
        str_address = edt_address.getText().toString();
        str_address1 = edt_address1.getText().toString();
        str_city = edt_city.getText().toString();
        str_pincode = edt_pincode.getText().toString();
        str_phno = edt_phoneno.getText().toString();
        str_mailid = edt_mailid.getText().toString();
        str_openingbal=edt_openingbal.getText().toString();

 /*       if(str_address_s==null||str_address_s.isEmpty()){
            str_address_s="";
            str_address1_s="";
            str_city_s="";
            str_state_s="";
            str_country_s="";
            str_pincode_s="";
        }
        else if(str_address1_s.equals(null)||str_address1_s.isEmpty()){
            str_address1_s="";

        }
*/







       /* str_address_s=prefs.getShippingAddress1();
        str_address1_s=prefs.getShippingAddress2();
        str_city_s=prefs.getShippingCity();
        str_pincode_s=prefs.getShippingPincode();
        str_state_s=prefs.getShippingState();
        str_country_s=prefs.getShippingCountry();*/


        if (TextUtils.isEmpty(str_suppliername)) {
            edt_customername.requestFocus();
            edt_customername.setError("Supplier name can't be empty");

            return;
        }

        if(chk_nogstno.isChecked()==false) {
            if (str_gstno.length() < 15) {
                gst15.requestFocus();
                gst15.setError("Invalid Gst number");

                return;
            }

        }

        else if (TextUtils.isEmpty(str_address)) {
            edt_address.requestFocus();
            edt_address.setError("Supplier Address can't be empty");

            return;
        } else if (TextUtils.isEmpty(str_city)) {
            edt_city.requestFocus();
            edt_city.setError("Supplier City can't be empty");

            return;
        } else if (TextUtils.isEmpty(str_pincode)) {
            edt_city.requestFocus();
            edt_city.setError("Supplier Pincode can't be empty");

            return;
        }

        else if (str_mailid.length() > 0){

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(str_mailid).matches()) {
                Toast toast = new Toast(view.getContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toasttext.setText("Invalid email");
                toast.setView(layouttoast);
                toast.show();
                return;
            }

        }
        requestQueue = Volley.newRequestQueue(view.getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Adding Supplier..");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            String m=response.toString();
                            String x=     removeUTF8BOM(m);
                            JSONObject jsonRequest = new JSONObject(x);
                            String statuscode = jsonRequest.getString("status_code");
                            if (statuscode.equals("200")) {
                                String id = jsonRequest.getString("id");

                                supplierdetailHashMap = new ArrayList<HashMap<String,String>>();
                                HashMap<String, String> userhashmap = new HashMap<String, String>();
                                userhashmap.put("id",id);
                                userhashmap.put("supplier_name",str_suppliername);
                                userhashmap.put("gst_no", str_gstno);
                                userhashmap.put("user_id", prefs.getUserId());
                                userhashmap.put("address",str_address);
                                userhashmap.put("address1",str_address1);
                                userhashmap.put("city", str_city);
                                userhashmap.put("state",str_state);
                                userhashmap.put("pincode", str_pincode);
                                userhashmap.put("phone_no", str_phno);
                                userhashmap.put("mail_id", str_mailid);
                                userhashmap.put("spn_state_position", str_statepos);
                                userhashmap.put("opening_balance", str_openingbal);
                                userhashmap.put("opening_balance_crordr", str_crordr);

                                supplierdetailHashMap.add(userhashmap);
                                if(str_insertorupdate.equals("I")){
                                    invoicedb.insertSupplierdetails(supplierdetailHashMap);
                                }
                                else{
                                    invoicedb.updateSupplierdetails(supplierdetailHashMap);
                                }

                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                if(str_insertorupdate.equals("I")) {
                                    toasttext.setText("Supplier Added Successfully");
                                    gst1.setText("");
                                    gst2.setText("");
                                    gst3.setText("");
                                    gst4.setText("");
                                    gst5.setText("");
                                    gst6.setText("");
                                    gst7.setText("");
                                    gst8.setText("");
                                    gst9.setText("");
                                    gst10.setText("");
                                    gst11.setText("");
                                    gst12.setText("");
                                    gst13.setText("");
                                    gst15.setText("");
                                    edt_customername.setText("");
                                    edt_address.setText("");
                                    edt_city.setText("");
                                    edt_pincode.setText("");
                                    edt_phoneno.setText("");
                                    edt_mailid.setText("");
                                    spn_state.setSelection(0);
                                    edt_openingbal.setText("");
                                    edt_address1.setText("");
                                    str_address1_s="";
                                    str_address_s="";
                                    str_city_s="";
                                    str_state_s="";
                                    str_pincode_s="";
                                    str_country_s="";
                                }
                                else{
                                    toasttext.setText("Supplier Updated Successfully");
                                }

                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();
                                progressDialog.dismiss();

                            }

                            else    if (statuscode.equals("600")) {

                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText("Supplier with GST number already exists");
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();

                                progressDialog.dismiss();

                            }

                            else   if (statuscode.equals("400")){
                                Toast toast = new Toast(view.getContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(jsonRequest.getString("status"));
                                toast.setView(layouttoast);
                                progressDialog.dismiss();
                                toast.show();

                            }
                            //    Toast.makeText(getApplicationContext(), "" +  cityPost.getCities() ,Toast.LENGTH_LONG).show();


                        } catch (Exception e) {
                            Toast.makeText(getContext(), "" +  e ,Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    }
                },


                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(view.getContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layouttoast);
                        progressDialog.dismiss();
                        toast.show();
                       /* Toast.makeText(getApplicationContext(), "" + error, Toast.LENGTH_LONG).show();*/

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("supplier_name",str_suppliername);
                params.put("gst_no", str_gstno);
                params.put("user_id",prefs.getUserId());
                params.put("address",str_address);
                params.put("address1",str_address1);
                params.put("city",str_city);
                params.put("pincode",str_pincode);
                params.put("spn_state_position",str_statepos);
                if(str_gstno.equals("NA")) {
                   str_state=invoicedb.getvaluebystateposition(str_statepos);
                }
                params.put("state",str_state);
                params.put("ph_no",str_phno);
                params.put("mail_id",str_mailid);
                if(str_openingbal==null){
                    str_openingbal="0";
                }
                params.put("opening_bal",str_openingbal);
                params.put("crordr",str_crordr);
                params.put("insertorupdate",str_insertorupdate);
                if(str_insertorupdate.equals("U")){
                    params.put("updateid", str_supplier_id);

                }
                else{
                    params.put("updateid", "");
                }


                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }


    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }





    /*public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CODE && resultCode == Activity.RESULT_OK) {
            if(data != null) {
                String value = data.getStringExtra("SHIPPPING_ADDRESS1");
                String value1= data.getStringExtra("SHIPPPING_ADDRESS2");
                if(value != null) {
                    Log.v(TAG, "Data passed from Child fragment = " + value);
                }
            }
        }
    }*/
}

