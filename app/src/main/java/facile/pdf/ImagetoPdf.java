package facile.pdf;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import facile.Adapter.Invoiceitemsadapter;
import facile.Model.Bank_details_model;
import facile.Model.Invoice_Product_model;
import facile.Model.Invoice_calc;
import facile.Model.Invoice_details;
import facile.Model.User_detail;
import facile.Model.invoice_customer_model;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.Constants;
import facile.util.EnglishNumberToWords;
import facile.util.SimpleDividerItemDecoration;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 8/2/18.
 */

public class ImagetoPdf  extends AppCompatActivity {
    RelativeLayout relativeLayout;
    String dirpath;
    File imgfile;
    TextView tv_phoneno,tv_companyname,tv_address,tv_gstno,tv_date,tv_invoiceno,tv_customername,
            tv_customeraddress,tv_buyergstno,tv_placeofsupply,tv_tat,tv_cgst,tv_sgst,tv_igst,tv_tot,
            tv_bankname,tv_branch,tv_acno,tv_ifsc,tv_forcustomer,tv_freight,tv_othercharges,tv_amountinwords,tv_termsofuse,tv_roundofftxt;
    String str_phoneno,str_companyname,str_address,str_gstno,str_date,str_invoiceno,str_p_invoiceno_s,str_customername,
    str_customeraddress,str_customeraddress1,str_buyergstno,str_placeofsupply,str_tat,str_total,str_cgst,str_sgst,str_igst,
    str_salestype,str_transportmode,str_rc,str_customerphno,str_customercity,str_customerstate,str_customerstatepos,
        str_customermailid,str_customerigst,str_customerpincode,str_freight,str_other_charges,str_dateval,str_invoiceid;
    Button btn_save;
    Invoicedatabase invoicedb;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    RequestQueue requestQueue;
    public static final String UTF8_BOM = "\uFEFF";
    ProgressDialog progressDialog;
    String url;
    TextView toasttext;
    View layout;
    prefManager prefs;
    Context context;
    ImageView iv_companylogo;
    String str_persontype;
    Bitmap bitmap;
    String str_insertorupdate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invoice_layout);
        invoicedb = new Invoicedatabase(this);
        prefs = new prefManager(this);
        context = this;
       /* tv=(TextView)findViewById(R.id.txt_ex);*/
        relativeLayout = (RelativeLayout) findViewById(R.id.rlmain);
        btn_save = (Button) findViewById(R.id.btn_save);
        tv_phoneno = (TextView) findViewById(R.id.tv_phoneno);
        tv_companyname = (TextView) findViewById(R.id.tv_companyname);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_freight = (TextView) findViewById(R.id.tv_freight);
        tv_othercharges = (TextView) findViewById(R.id.tv_others);
        tv_gstno = (TextView) findViewById(R.id.tv_gstnotxt);
        tv_invoiceno = (TextView) findViewById(R.id.tv_invoicenotxt);
        tv_date = (TextView) findViewById(R.id.tv_datetxt);
        tv_customername = (TextView) findViewById(R.id.tv_customername);
        tv_customeraddress = (TextView) findViewById(R.id.tv_customeraddress);
        tv_buyergstno = (TextView) findViewById(R.id.tv_buyergstnotxt);
        tv_placeofsupply = (TextView) findViewById(R.id.tv_placeofsupplytxt);
        tv_tot = (TextView) findViewById(R.id.tv_totalbeforetaxtxt);
        tv_amountinwords = (TextView) findViewById(R.id.txt_amountinwordstxt);
        tv_termsofuse=(TextView)findViewById(R.id.tv_termsofusetxt) ;
        tv_cgst = (TextView) findViewById(R.id.tv_cgsttxt);
        tv_sgst = (TextView) findViewById(R.id.tv_sgsttxt);
        tv_igst = (TextView) findViewById(R.id.tv_igsttxt);
        tv_tat = (TextView) findViewById(R.id.tv_totaltxt);
        tv_roundofftxt=(TextView) findViewById(R.id.tv_roundofftxt);
        tv_bankname = (TextView) findViewById(R.id.tv_banknametxt);
        tv_branch = (TextView) findViewById(R.id.tv_branchnametxt);
        tv_acno = (TextView) findViewById(R.id.tv_acnotxt);
        tv_ifsc = (TextView) findViewById(R.id.tv_ifsctxt);
        iv_companylogo = (ImageView) findViewById(R.id.iv_companylogo);
        tv_forcustomer = (TextView) findViewById(R.id.tv_forcustomer);
        LayoutInflater li = getLayoutInflater();
        //Getting the View object as defined in the customtoast.xml file
        layout = li.inflate(R.layout.customtoast,
                (ViewGroup) findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layout.findViewById(R.id.custom_toast_message);
        ArrayList<User_detail> items = invoicedb.getuserdetails();
        tv_companyname.setText(items.get(0).user_trade_name);
        tv_gstno.setText(items.get(0).gst_no);

        if (items.get(0).address1.isEmpty() || items.get(0).address1 == null) {
            tv_address.setText(items.get(0).address + "\n" + items.get(0).city + "," + items.get(0).state
                    + "-" + items.get(0).pincode);
        } else {
            tv_address.setText(items.get(0).address + "\n" + items.get(0).address1 + "\n" + items.get(0).city + "," + items.get(0).state
                    + "-" + items.get(0).pincode);

        }
        tv_phoneno.setText(items.get(0).phone_no);
        tv_placeofsupply.setText(items.get(0).state);
        tv_forcustomer.setText(items.get(0).user_trade_name);
        tv_termsofuse.setText(items.get(0).terms_of_use);
        ArrayList<Bank_details_model> bankdetails = invoicedb.getbankdetails();

        if(bankdetails.size()>0) {

            tv_bankname.setText(bankdetails.get(0).bank_name);
            tv_branch.setText(bankdetails.get(0).branch);
            tv_ifsc.setText(bankdetails.get(0).ifsc);
            tv_acno.setText(bankdetails.get(0).ac_no);
        }
        Intent intent = getIntent();
        invoice_customer_model customer = intent.getParcelableExtra("customer");
        Invoice_details invoice = intent.getParcelableExtra("invoice");
        Invoice_calc calc = intent.getParcelableExtra("invoice_calc");

        if(intent.hasExtra("insertorupdate")){

            str_invoiceid=intent.getStringExtra("invoice_id");
            str_insertorupdate="U";

        }
        else{
            str_insertorupdate="I";
        }

        str_customername = customer.customer_name;
        str_customeraddress = customer.address;
        str_customeraddress1 = customer.address1;
        str_buyergstno = customer.gst_no;
        str_customerphno = customer.phone_no;
        str_customerstate = customer.state;
        str_customerstatepos = customer.state_pos;
        str_customercity = customer.city;
        str_customermailid = customer.mail_id;
        str_customerigst = customer.igst;
        str_customerpincode = customer.pincode;
        str_persontype=invoice.person_type;
        str_date = invoice.invoice_date;
        str_invoiceno = invoice.invoice_no;
        str_p_invoiceno_s = invoice.p_invoice_no_s;
        str_salestype = invoice.sales_type;
        str_transportmode = invoice.transport_mode;
        str_rc = invoice.reverse_charge;
        tv_invoiceno.setText(str_p_invoiceno_s);
        str_dateval = invoice.str_dateval;
        str_cgst = calc.cgst;
        str_sgst = calc.sgst;
        str_igst = calc.igst;


        String[] arr = String.valueOf(calc.total_after_tax).split("\\.");
        int[] intArr = new int[2];
        intArr[0] = Integer.parseInt(arr[0]);
      intArr[1] = Integer.parseInt(arr[1]);

        long tat=Math.round(Double.parseDouble(calc.total_after_tax));

        str_tat = String.valueOf( intArr[0] );
        tv_roundofftxt.setText("0."+ intArr[1]);



        str_total = calc.total_before_tax;
        str_freight = calc.freight;
        str_other_charges = calc.other_charges;
      /*  String[] arr = String.valueOf(str_tat).split("\\.");
        int[] intArr = new int[2];*/
     /*   intArr[0] = Integer.parseInt(arr[0]);*/
      /*  intArr[1] = Integer.parseInt(arr[1]);*/
        Long amnt_whole_no = Long.valueOf(intArr[0]);
        Long amnt_decimal_no = Long.valueOf(intArr[1]);
        String amntinwrds = EnglishNumberToWords.convert(amnt_whole_no);
     /*   String decwrds = EnglishNumberToWords.convert(amnt_decimal_no);*/
        String rupees_amnt = amntinwrds.substring(0, 1).toUpperCase() + amntinwrds.substring(1);

      /*  if (intArr[1] == 0) {*/
            tv_amountinwords.setText(rupees_amnt + " rupees  only");
/*
        } else {


            tv_amountinwords.setText(rupees_amnt + " rupees and " + decwrds + " paise only");
        }*/
        tv_customername.setText(str_customername);

        if ((str_customeraddress == null) || str_customeraddress.isEmpty()) {
            tv_customeraddress.setText(str_customerstate);
        } else if ((str_customeraddress1 == null) || str_customeraddress1.isEmpty()) {
            tv_customeraddress.setText(str_customeraddress + "\n" + str_customercity + "," + str_customerstate
                    + "-" + str_customerpincode);
        } else {
            tv_customeraddress.setText(str_customeraddress + "," + str_customeraddress1 + "\n" + str_customercity + "," + str_customerstate
                    + "-" + str_customerpincode);
        }
        tv_buyergstno.setText(str_buyergstno);
        tv_date.setText(str_date);


        if (str_freight == null || str_freight.isEmpty()) {
            tv_freight.setText("0.0");
        } else{
        tv_freight.setText(str_freight);
    }
        if (str_other_charges == null || str_other_charges.isEmpty()) {
            tv_othercharges.setText("0.0");
        } else{
            tv_othercharges.setText(str_other_charges);
        }

        tv_cgst.setText(str_cgst);
        tv_sgst.setText(str_sgst);
        tv_igst.setText(str_igst);

        tv_tat.setText(str_tat);
        tv_tot.setText(str_total);
        try {
            imgfile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/BizManager/profilepic.png");
            if(imgfile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgfile.getAbsolutePath());
                iv_companylogo.setImageBitmap(myBitmap);

            }
        }
        catch(Exception e){

        }
        mRecyclerView = (RecyclerView)findViewById(R.id.rv_proddetail);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new Invoiceitemsadapter(this,invoicedb.getaddproductitems());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(str_insertorupdate.equals("U")){
                    url = Constants.INVOICE_MAIN_UPDATE;
                }else {
                    url = Constants.INVOICE_MAIN;
                }


                requestQueue = Volley.newRequestQueue(getApplicationContext());
                progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
                progressDialog.setMessage("Saving invoice details..");
                progressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    String m=response.toString();
                                    String x=     removeUTF8BOM(m);
                                    JSONObject jsonRequest = new JSONObject(x);
                                    String statuscode = jsonRequest.getString("status_code");
                                    if (statuscode.equals("200")) {
                                        Bitmap map = ConvertToBitmap(relativeLayout);
                                        /* saveToInternalStorage(map);*/
                                        saveToExternalStorage(map);
                                   }
                                    else   if (statuscode.equals("400")){
                                        Toast toast = new Toast(getApplicationContext());
                                        toast.setDuration(Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                        toasttext.setText(jsonRequest.getString("status"));
                                        toast.setView(layout);
                                        progressDialog.dismiss();
                                        toast.show();

                                    }
                                    else   if (statuscode.equals("800")){
                                        Toast toast = new Toast(getApplicationContext());
                                        toast.setDuration(Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                        toasttext.setText(jsonRequest.getString("status"));
                                        toast.setView(layout);
                                        progressDialog.dismiss();
                                        toast.show();

                                    }

                                    //    Toast.makeText(getApplicationContext(), "" +  cityPost.getCities() ,Toast.LENGTH_LONG).show();


                                } catch (Exception e) {
                           /* Toast.makeText(getApplicationContext(), "" +  e ,Toast.LENGTH_LONG).show();*/
                                    progressDialog.dismiss();


                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                errorhandler e=new errorhandler();
                                String msg=e.errorhandler(error);
                                Toast toast = new Toast(getApplicationContext());
                                toast.setDuration(Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                toasttext.setText(msg);
                                toast.setView(layout);
                                progressDialog.dismiss();
                                toast.show();


                            }
                        }) {


                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();

                    //invoice details params
                        params.put("invoice_no",str_invoiceno);
                        params.put("p_invoice_no_s",str_p_invoiceno_s);
                        params.put("user_id",prefs.getUserId());
                        params.put("sales_type",str_salestype);
                        params.put("transport_mode",str_transportmode);
                        params.put("invoice_date",str_dateval);
                        params.put("rc",str_rc);
                        params.put("person_type",str_persontype);
                        params.put("business_type",prefs.getBusinessType());

                        if(str_insertorupdate.equals("U")){
                            params.put("invoice_id",str_invoiceid);
                        }


                    //customer details params

                        params.put("customer_name",str_customername);
                        params.put("gst_no",str_buyergstno);
                        params.put("address",str_customeraddress);
                        params.put("address1",str_customeraddress1);
                        params.put("phone_no",str_customerphno);
                        params.put("mail_id",str_customermailid);
                        params.put("city",str_customercity);
                        params.put("state",str_customerstate);
                        params.put("state_pos",str_customerstatepos);
                        params.put("customer_igst",str_customerigst);
                        params.put("pincode",str_customerpincode);

                     // Product details params
                     ArrayList<Invoice_Product_model> items= invoicedb.getaddproductitems();
                        for(int i=0; i < items.size(); i++)
                        {
                            params.put("product_code["+i+"]",items.get(i).productcode);
                            params.put("product_desc["+i+"]",items.get(i).productdesc);
                            params.put("unit["+i+"]",items.get(i).unit);
                            params.put("hsn_code["+i+"]",items.get(i).hsncode);
                            params.put("gst_rate["+i+"]",items.get(i).gstrate);
                            params.put("cess["+i+"]",items.get(i).cess);
                            params.put("quantity["+i+"]",items.get(i).quantity);
                            params.put("amount["+i+"]",items.get(i).amount);
                            params.put("discount["+i+"]",items.get(i).discount);
                            params.put("discount_type["+i+"]",items.get(i).discountype);
                            params.put("totalbeforetax["+i+"]",items.get(i).grossamount);
                            params.put("cgst["+i+"]",items.get(i).cgst);
                            params.put("sgst["+i+"]",items.get(i).sgst);
                            params.put("igst["+i+"]",items.get(i).igst);
                            params.put("cgst_percent["+i+"]",items.get(i).cgst_percent);
                            params.put("sgst_percent["+i+"]",items.get(i).sgst_percent);
                            params.put("igst_percent["+i+"]",items.get(i).igst_percent);
                            params.put("tat["+i+"]",items.get(i).tat);
                        }
             //calc params
                        params.put("total_before_tax_f",str_total);
                        params.put("cgst_f",str_cgst);
                        params.put("sgst_f",str_sgst);
                        params.put("igst_f",str_igst);
                        params.put("freight",str_freight);
                        params.put("other_charges",str_other_charges);
                        params.put("tat_f",str_tat);
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                        return params;
                    }

                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);






            }
        });


    }

    public void layoutToImage(Bitmap bmp) {

            try {
                imageToPDF();
            }
            catch (Exception e){

            }

    }


    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(this);
        File mypath;
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("Invoice", Context.MODE_PRIVATE);
        // Create imageDir
     /*   if(flag==1) {
            mypath = new File(directory, "shareimgcache.png");
        }
        else{*/
            mypath = new File(directory, System.currentTimeMillis()+"image.png");

       // }

     /*   File mypath=new File(directory,"profile.jpg");*/

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);

          /*  addImageToGallery(mypath.getPath(),context);*/
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                imageToPDF();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }


    public void imageToPDF() throws FileNotFoundException {
        String Filename="";
        try {
            Document document = new Document();
            dirpath = android.os.Environment.getExternalStorageDirectory().toString();
            Filename= "/BizManager/Invoice/"+str_invoiceno+".pdf";
            PdfWriter.getInstance(document, new FileOutputStream(dirpath +Filename)); //  Change pdf's name.
            document.open();
            Image img = Image.getInstance(Environment.getExternalStorageDirectory() + File.separator + "/BizManager/image.png");
            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                    - document.rightMargin() - 0) / img.getWidth()) * 100;
            img.scalePercent(scaler);
            img.setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
            document.add(img);
            document.close();
            progressDialog.dismiss();
            Toast.makeText(this, "Invoice saved successfully!..", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Invoice Generation failed!.."+e, Toast.LENGTH_SHORT).show();

        }
        finally {

          Intent i=new Intent(ImagetoPdf.this,PrintandShare.class);
            i.putExtra("filename",Filename);
            startActivity(i);
            finish();

           /* File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +Filename);

            Intent target = new Intent(Intent.ACTION_VIEW);
            target.setDataAndType(Uri.fromFile(file),"application/pdf");
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent = Intent.createChooser(target, "Open File");
           *//* Intent intent = new Intent();
            intent.setPackage("com.adobe.reader");
            intent.setDataAndType(Uri.fromFile(file), "application/pdf");
*//*
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {

                Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_SHORT).show();
                // Instruct the user to install a PDF reader here, or something


            }*/
        }
    }

    private String saveToExternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File mypath;

        File filepath = Environment.getExternalStorageDirectory();

        // Create a new folder in SD Card
        File dir = new File(filepath.getAbsolutePath()
                + "/BizManager/");
        dir.mkdirs();

        File dir1 = new File(filepath.getAbsolutePath()
                + "/BizManager/Invoice/");
        dir1.mkdirs();

            mypath = new File(dir, "image.png");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                fos.close();
                imageToPDF();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dir.getAbsolutePath();
    }

    protected Bitmap ConvertToBitmap(RelativeLayout layout) {

        layout.setDrawingCacheEnabled(true);
        layout.buildDrawingCache();
        return  layout.getDrawingCache();


    }
    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }


}
