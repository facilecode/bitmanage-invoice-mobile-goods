package facile.pdf;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import facile.invoice.MainActivity;
import facile.invoice.R;

/**
 * Created by pradeep on 13/4/18.
 */

public class PrintandShare  extends AppCompatActivity {

    ImageView iv_invoice;
    Button btn_print,btn_share,btn_email;
    String Filename;
    File imgfile;
    Context context;
    File file;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.printandshare);
        context=this;
        iv_invoice=(ImageView)findViewById(R.id.iv_invoice);
        Intent intent = getIntent();
        Filename=intent.getStringExtra("filename");


        imgfile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/BizManager/image.png");
        btn_print=(Button)findViewById(R.id.btn_print);
        btn_share=(Button)findViewById(R.id.btn_share);
        btn_email=(Button)findViewById(R.id.btn_email);
        if(imgfile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgfile.getAbsolutePath());
            iv_invoice.setImageBitmap(myBitmap);

        }
         file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +Filename);
        btn_print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            Intent target = new Intent(Intent.ACTION_VIEW);
            target.setDataAndType(Uri.fromFile(file),"application/pdf");
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent = Intent.createChooser(target, "Open File");
     try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {

                Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_SHORT).show();
                // Instruct the user to install a PDF reader here, or something


            }

            }
        });
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("application/pdf");
                sharingIntent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
                sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
                context.startActivity(Intent.createChooser(sharingIntent, "Share Image Using"));


            }
        });
        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Invoice");
                Uri uri = Uri.fromFile(file);
                emailIntent.putExtra(Intent.EXTRA_STREAM, uri);

                emailIntent.putExtra(Intent.EXTRA_TEXT, "Please find the attached pdf for invoice");
                emailIntent.setType("message/rfc822");

                startActivity(Intent.createChooser(emailIntent, "Choose an Email client :"));


                try {
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));


                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(PrintandShare.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }









            }
        });





    }


    @Override
    public void onBackPressed() {
        Intent i=new Intent(PrintandShare.this,MainActivity.class);

        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();

    }


}
