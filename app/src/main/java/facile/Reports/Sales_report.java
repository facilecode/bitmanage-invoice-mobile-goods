package facile.Reports;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import facile.invoice.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Sales_report.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Sales_report#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Sales_report extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
String str_sorp;

    Button btn_report_closure,btn_monthwisereport,btn_yearwisereport,btn_saleschart;

    private Sales_report.OnFragmentInteractionListener mListener;

    public Sales_report() {
        // Required empty public constructora
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Sales_report.
     */
    // TODO: Rename and change types and number of parameters
    public static Sales_report newInstance(String param1, String param2) {
        Sales_report fragment = new Sales_report();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_sales_report, container, false);

        btn_report_closure=(Button)view.findViewById(R.id.btn_reportcloasure);
        btn_monthwisereport=(Button)view.findViewById(R.id.btn_monthwisereport);
        btn_yearwisereport=(Button)view.findViewById(R.id.btn_datewisereport);
        btn_saleschart=(Button)view.findViewById(R.id.btn_charts);
        Bundle bundle = this.getArguments();
        str_sorp = bundle.getString("SORP", "");
        if(str_sorp.equals("P")){
            btn_saleschart.setText("Purchase Chart");
        }


        btn_report_closure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Report_closure fragment = new Report_closure();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("SORP",str_sorp);
                fragment.setArguments(bundle);
                fragmentTransaction.commit();


            }
        });
        btn_monthwisereport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Month_wise_report fragment = new Month_wise_report();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("SORP",str_sorp);
                bundle.putString("str_type","S");
                fragment.setArguments(bundle);
                fragmentTransaction.commit();

            }
        });
        btn_yearwisereport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Date_wise_report fragment = new Date_wise_report();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("SORP",str_sorp);
                fragment.setArguments(bundle);
                fragmentTransaction.commit();

            }
        });
        btn_saleschart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Chart_report_main fragment = new Chart_report_main();
                FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("SORP",str_sorp);
                fragment.setArguments(bundle);
                fragmentTransaction.commit();

            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
