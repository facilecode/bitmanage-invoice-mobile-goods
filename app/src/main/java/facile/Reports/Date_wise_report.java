package facile.Reports;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.DatePickerFragmentFromTo;

/**
 * Created by pradeep on 23/3/18.
 */

public class Date_wise_report extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    EditText edt_from,edt_to;
    Button btn_report;
    String str_from,str_to;
    Invoicedatabase invoicedatabase;
    static String dateString;
    String str_crordr,str_type,str_cr_sorp,str_sorp;

    private Date_wise_report.OnFragmentInteractionListener mListener;

    public
    Date_wise_report() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Report_closure.
     */
    // TODO: Rename and change types and number of parameters
    public static Month_wise_report newInstance(String param1, String param2) {
        Month_wise_report fragment = new Month_wise_report();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.date_wise_report, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.datewise_report));
        edt_from=(EditText)view.findViewById(R.id.edt_from);
        edt_to=(EditText)view.findViewById(R.id.edt_to);
        btn_report=(Button)view.findViewById(R.id.btn_report);
        Bundle bundle = this.getArguments();
        if(bundle!=null) {
            if (bundle.containsKey("CRORDR")) {
                str_crordr = bundle.getString("CRORDR", "");
                str_type = bundle.getString("str_type", "");
                if (str_crordr.equals("DR")) {
                    str_cr_sorp = bundle.getString("DR_SORP", "");
                }
            } else if (bundle.containsKey("SORP")) {
                str_sorp = bundle.getString("SORP", "");
                str_type = bundle.getString("str_type", "");
            }

        }

            Calendar aCalendar = Calendar.getInstance();
        if(bundle!=null){
            aCalendar.add(Calendar.MONTH, 1);
        }else {
            aCalendar.add(Calendar.MONTH, -1);
        }

        aCalendar.set(Calendar.DATE, 1);

        aCalendar.set(Calendar.DATE, aCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));

        Date lastDateOfPreviousMonth = aCalendar.getTime();


        SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
        dateFormat.applyPattern("dd/MM/yy");
         dateString = dateFormat.format(lastDateOfPreviousMonth);

        edt_from.setText(dateString);
        edt_to.setText(dateString);

        edt_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showDatePicker("from");

            }
        });
        edt_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showDatePicker("to");

            }
        });

        btn_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(str_crordr==null||str_crordr.isEmpty()) {
                    Datewisereportview fragment = new Datewisereportview();
                    FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("str_from", edt_from.getText().toString());
                    bundle.putString("str_to", edt_to.getText().toString());
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();
                }
                else{
                    CrDr_date_wise_report_view fragment = new CrDr_date_wise_report_view();
                    FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.addToBackStack(null);
                    Bundle bundle = new Bundle();
                    bundle.putString("str_from", edt_from.getText().toString());
                    bundle.putString("str_to", edt_to.getText().toString());
                    bundle.putString("CRORDR", str_crordr);
                    fragment.setArguments(bundle);
                    fragmentTransaction.commit();

                }
            }
        });


        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onResume() {
        super.onResume();

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void showDatePicker(String fromto) {
        DatePickerFragmentFromTo date = new DatePickerFragmentFromTo();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));


            args.putString("Invoicemaxdate", dateString);


        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
if(fromto.equals("from")) {
    date.setCallBack(ondate);
}
else{
    date.setCallBack(ondateto);
}

        date.show(getFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
                dateFormat.applyPattern("dd/MM/yy");
                str_from = dateFormat.format(calendar.getTime());
                edt_from.setText(str_from);



                //str_dateval=dateFormat1.format(calendar.getTime());
            }
            catch (Exception e){

            }


        }
    };



    DatePickerDialog.OnDateSetListener ondateto = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {

                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
                dateFormat.applyPattern("dd/MM/yy");
                str_to = dateFormat.format(calendar.getTime());
                edt_to.setText(str_to);



                //str_dateval=dateFormat1.format(calendar.getTime());
            }
            catch (Exception e){

            }


        }
    };








}
