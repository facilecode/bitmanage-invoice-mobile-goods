package facile.util;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;

/**
 * Created by pradeep  on 23-Sep-16.
 */
public class errorhandler {


   public String errorhandler(com.android.volley.VolleyError error) {
        String message = null;



        if (error instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
           /* Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();*/

        } else if (error instanceof ServerError) {
            message = "The server is busy. Please try again after some time!!";
        } else if (error instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (error instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (error instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (error instanceof TimeoutError) {
            message = "Connection TimeOut! Please try again.";
        }
       return message;
    }


}
