package facile.util;
/**
 * Created by pradeep on 30/9/17.
 */
public class Constants {


 /*public static final String SIGNIN_URL = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/signin.php";
 public static final String USER_DETAILS = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/user_details.php";
 public static final String ADD_CUSTOMER = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/customer_details.php";
 public static final String ADD_SUPPLIER= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/supplier_details.php";
 public static final String ADD_ITEM = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/product_details.php";
 public static final String DELETE_SUPPLIER = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/delete_supplier.php";
 public static final String DELETE_CUSTOMER = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/delete_customer.php";
 public static final String DELETE_PRODUCT = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/delete_product.php";
 public static final String INVOICE_NUMBER = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/invoice_number.php";
 public static final String INVOICE_MAIN= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/invoice_main.php";
 public static final String PURCHASE_MAIN= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/purchase_main.php";
 public static final String DEBIT_NOTE_NUMBER_PURCHASE = "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/debit_note_number_purchase.php";
    public static final String DEBIT_NOTE_PURCHASE_MAIN= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/debit_note_purchase_main.php";
 public static final String INVOICE_MAIN_UPDATE= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/update_invoice_details.php";
 public static final String PURCHASE_MAIN_UPDATE= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/update_purchase_details.php";
 public static final String CHANGE_PASSWORD= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/change_password.php";
 public static final String REPORT_CLOSURE= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/Report_closure.php";
 public static final String REPORT_CLOSURE_PURCHASE= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/Report_closure_purchase.php";
 public static final String MONTH_WISE_REPORT= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/month_wise_report.php";
 public static final String MONTH_WISE_PURCHASE_REPORT= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/month_wise_purchase_report.php";
 public static final String CRDR_MONTH_WISE_REPORT= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/CN_DN_month_wise_report.php";
 public static final String PURCHASE_DR_MONTH_WISE_REPORT= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/purchase_DN_month_wise_report.php";
 public static final String CRDR_DATE_WISE_REPORT= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/CN_DN_date_wise_report.php";
 public static final String DATE_WISE_REPORT= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/date_wise_report.php";
 public static final String NON_CLOSURE_REPORT= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/Non_closure_report.php";
 public static final String NON_CLOSURE_REPORT_PURCHASE= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/Non_closure_purchase_report.php";
 public static final String GET_INVOICE_DETAILS= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/get_invoice_details.php";
 public static final String GET_PURCHASE_DETAILS= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/get_purchase_details.php";
 public static final String CREDIT_NOTE_NUMBER= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/credit_note_number.php";
 public static final String DEBIT_NOTE_NUMBER= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/debit_note_number.php";
 public static final String DEBIT_NOTE_MAIN= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/debit_note_main.php";
 public static final String CREDIT_NOTE_MAIN= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/credit_note_main.php";
 public static final String BANK_DETAILS= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/bank_details.php";
 public static final String MONTH_WISE_CHART= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/monthly_chart.php";
 public static final String MONTH_WISE_PURCHASE_CHART= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/monthly_purchase_chart.php";
 public static final String WEEKLY_CHART= "http://ec2-13-126-196-31.ap-south-1.compute.amazonaws.com/invoice/weekly_chart.php";*/




  public static final String SIGNIN_URL = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/signin.php";
 public static final String USER_DETAILS = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/user_details.php";
 public static final String ADD_CUSTOMER = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/customer_details.php";
 public static final String ADD_SUPPLIER= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/supplier_details.php";
 public static final String ADD_ITEM = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/product_details.php";
 public static final String DELETE_SUPPLIER = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/delete_supplier.php";
 public static final String DELETE_CUSTOMER = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/delete_customer.php";
 public static final String DELETE_PRODUCT = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/delete_product.php";
 public static final String INVOICE_NUMBER = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/invoice_number.php";
 public static final String INVOICE_MAIN= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/invoice_main.php";
 public static final String PURCHASE_MAIN= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/purchase_main.php";
 public static final String DEBIT_NOTE_NUMBER_PURCHASE = "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/debit_note_number_purchase.php";
 public static final String DEBIT_NOTE_PURCHASE_MAIN= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/debit_note_purchase_main.php";
 public static final String INVOICE_MAIN_UPDATE= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/update_invoice_details.php";
 public static final String PURCHASE_MAIN_UPDATE= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/update_purchase_details.php";
 public static final String CHANGE_PASSWORD= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/change_password.php";
 public static final String REPORT_CLOSURE= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/Report_closure.php";
 public static final String REPORT_CLOSURE_PURCHASE= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/Report_closure_purchase.php";
 public static final String MONTH_WISE_REPORT= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/month_wise_report.php";
 public static final String MONTH_WISE_PURCHASE_REPORT= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/month_wise_purchase_report.php";
 public static final String DATE_WISE_REPORT= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/date_wise_report.php";
 public static final String PURCHASE_DR_MONTH_WISE_REPORT= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/purchase_DN_month_wise_report.php";
 public static final String CRDR_MONTH_WISE_REPORT= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/CN_DN_month_wise_report.php";
 public static final String CRDR_DATE_WISE_REPORT= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/CN_DN_date_wise_report.php";
 public static final String NON_CLOSURE_REPORT= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/Non_closure_report.php";
 public static final String NON_CLOSURE_REPORT_PURCHASE= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/Non_closure_purchase_report.php";
 public static final String GET_INVOICE_DETAILS= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/get_invoice_details.php";
 public static final String GET_PURCHASE_DETAILS= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/get_purchase_details.php";
 public static final String CREDIT_NOTE_NUMBER= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/credit_note_number.php";
 public static final String CREDIT_NOTE_MAIN= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/credit_note_main.php";
 public static final String DEBIT_NOTE_NUMBER= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/debit_note_number.php";
 public static final String DEBIT_NOTE_MAIN= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/debit_note_main.php";
 public static final String BANK_DETAILS= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/bank_details.php";
 public static final String MONTH_WISE_CHART= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/monthly_chart.php";
 public static final String MONTH_WISE_PURCHASE_CHART= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/monthly_purchase_chart.php";
 public static final String WEEKLY_CHART= "http://ec2-35-173-143-52.compute-1.amazonaws.com/bizmanager/weekly_chart.php";

}

