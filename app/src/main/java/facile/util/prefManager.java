package facile.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by pradeep on 24/9/17.
 */

public class prefManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "invoiceprefs";

    private static final String IS_LOGGED_IN = "1";

    private static final String LOG_IN_TYPE= "logintype";
    private static final String USER_ID= "userid";
    private static final String STATE= "state";
    private static final String  LOGO= "logo";
    private static final String  INVOICE_NO= "invoiceno";
    private static final String  INVOICE_MIN_DATE= "invoicemindate";

    private static final String  COMPANY_NAME= "companyname";
    private static final String  PREFIX= "prefix";
    private static final String  SUFFIX= "suffix";

    private static final String  SALES= "sales";
    private static final String  REVERSE_CHARGE= "rc";
    private static final String  CONSUMER_STATE= "consumerstate";
    private static final String  BUSINESS_NATURE= "businessnature";
    private static final String  SERVICE_TYPE= "servicetype";
    private static final String  BUSINESS_TYPE= "businesstype";
    private static final String  RETAIL= "0";




    public prefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }



    public  String getIsLoggedIn() {
        return pref.getString(IS_LOGGED_IN, "1");
    }
    public void setIsLoggedIn(String isLoggedIn) {
        editor.putString(IS_LOGGED_IN, isLoggedIn);
        editor.commit();
    }


    public void setLogInType(String type) {
        editor.putString(LOG_IN_TYPE, type);
        editor.commit();
    }



    public String getLogInType() {
        return pref.getString(LOG_IN_TYPE, "");
    }

    public String getUserId() {
        return pref.getString(USER_ID, "");
    }

    public void setUserId(String id) {
        editor.putString(USER_ID, id);
        editor.commit();
    }
    public  String getState() {
        return pref.getString(STATE, "");
    }
    public void setState(String state) {
        editor.putString(STATE, state);
        editor.commit();
    }
    public  String getLogo() {
        return pref.getString(LOGO, "");
    }
    public void setLogo(String logo) {
        editor.putString(LOGO, logo);
        editor.commit();
    }
    public  String getCompanyName() {
        return pref.getString(COMPANY_NAME, "");
    }
    public void setCompanyName(String companyName) {
        editor.putString(COMPANY_NAME, companyName);
        editor.commit();
    }

    public  String getInvoiceNo() {
        return pref.getString(INVOICE_NO, "");
    }
    public void setInvoiceNo(String invoiceNo) {
        editor.putString(INVOICE_NO, invoiceNo);
        editor.commit();
    }
    public  String getInvoiceMinDate() {
        return pref.getString(INVOICE_MIN_DATE, "");
    }
    public void setInvoiceMinDate(String invoiceMinDate) {
        editor.putString(INVOICE_MIN_DATE, invoiceMinDate);
        editor.commit();
    }
    public  String getPrefix() {
        return pref.getString(PREFIX, "");
    }
    public void setPrefix(String prefix) {
        editor.putString(PREFIX, prefix);
        editor.commit();
    }
    public  String getSuffix() {
        return pref.getString(SUFFIX, "");
    }
    public void setSuffix(String suffix) {
        editor.putString(SUFFIX, suffix);
        editor.commit();
    }


    public  String getSales() {
        return pref.getString(SALES, "1");
    }
    public void setSales(String sales) {
        editor.putString(SALES, sales);
        editor.commit();
    }
    public  String getReverseCharge() {
        return pref.getString(REVERSE_CHARGE, "NO");
    }
    public void setReverseCharge(String rc) {
        editor.putString(REVERSE_CHARGE, rc);
        editor.commit();
    }

    public  String getRetail() {
        return pref.getString(RETAIL, "0");
    }
    public void setRetail(String retail) {
        editor.putString(RETAIL, retail);
        editor.commit();
    }


    public  String getConsumerState() {
        return pref.getString(CONSUMER_STATE, "31");
    }
    public void setConsumerState(String consumerState) {
        editor.putString(CONSUMER_STATE, consumerState);
        editor.commit();
    }
    public  String getBusinessNature() {
        return pref.getString(BUSINESS_NATURE, "1");
    }
    public void setBusinessNature(String businessNature) {
        editor.putString(BUSINESS_NATURE, businessNature);
        editor.commit();
    }
    public  String getServiceType() {
        return pref.getString(SERVICE_TYPE, "1");
    }
    public void setServiceType(String serviceType) {
        editor.putString(SERVICE_TYPE, serviceType);
        editor.commit();
    }


    public  String getBusinessType() {
        return pref.getString(BUSINESS_TYPE, "1");
    }
    public void setBusinessType(String businessType) {
        editor.putString(BUSINESS_TYPE, businessType);
        editor.commit();
    }



}
