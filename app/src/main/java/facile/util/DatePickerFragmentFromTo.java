package facile.util;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pradeep on 23/3/18.
 */

public class DatePickerFragmentFromTo extends DialogFragment {
    DatePickerDialog.OnDateSetListener ondateSet;
    public DatePickerFragmentFromTo() {
    }
    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }
    private int year, month, day;
    private String maxdate;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        year = args.getInt("year");
        month = args.getInt("month");
        day = args.getInt("day");
        maxdate=args.getString("Invoicemaxdate");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DatePickerDialog d= new DatePickerDialog(getActivity(), ondateSet, year, month, day);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        try {
            Date date = dateFormat.parse(maxdate);
            long m = date.getTime();
            d.getDatePicker().setMaxDate(m);
        }
        catch (Exception e){

        }
        return d;
    }
}
