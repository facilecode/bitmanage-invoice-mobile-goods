package facile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.sales_model;
import facile.invoice.R;


public class SalesgridAdapter extends BaseAdapter {

    private final ArrayList<sales_model> saleslist;
    private Context context;

    LayoutInflater mlaLayoutInflater = null;
    private static final int CORNER_RADIUS = 24; // dips
    private static final int MARGIN = 12; // dips
    private final int mCornerRadius;
    private final int mMargin;
  /*  private final int height;
    private final int width;*/



    public SalesgridAdapter(Context context, ArrayList<sales_model> saleslist) {
        this.saleslist = saleslist;
        this.context = context;
        final float density = context.getResources().getDisplayMetrics().density;
        mCornerRadius = (int) (CORNER_RADIUS * density + 0.5f);
        mMargin = (int) (MARGIN * density + 0.5f);

/*
         height = context.getResources().getDisplayMetrics().heightPixels;
         width =context.getResources().getDisplayMetrics().widthPixels;*/


        mlaLayoutInflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return saleslist.size();
    }

    @Override
    public Object getItem(int position) {
        return saleslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public class Holder1 {

        TextView categories;
        ImageView iv_sales;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder1 holder = new Holder1();
        final View rowView;
        rowView = mlaLayoutInflater.inflate(R.layout.sales_grid_row, null);
        holder.categories = (TextView)rowView.findViewById(R.id.txt_category);
        holder.iv_sales = (ImageView)rowView.findViewById(R.id.txt_cat);
        holder.categories.setText(saleslist.get(position).sales_title);
        holder.iv_sales.setImageResource(saleslist.get(position).salesimg);
        return rowView;

    }


}
