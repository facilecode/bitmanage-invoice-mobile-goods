package facile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.Month_model;
import facile.invoice.R;

/**
 * Created by pradeep on 10/10/17.
 */
public class MonthlistArrayAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<Month_model> items;
    private final int mResource;

    public MonthlistArrayAdapter(Context context, int resource,
                                 ArrayList<Month_model> objects) {


        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView statename = (TextView) view.findViewById(R.id.tv_statename);
        TextView statecode = (TextView) view.findViewById(R.id.tv_statecode);

        statename.setText(items.get(position).month);
        statecode.setText(items.get(position).id);

        statename.setTextSize(12);
        return view;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}

