package facile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import facile.invoice.R;

/**
 * Created by pradeep on 31/03/18.
 */

public class Invoice_list_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private ArrayList<HashMap<String, String>> mDataset;
    private static final String INVOICE_NO = "invoiveno";
    private static final String CUSTOMER_NAME = "customer_name";
    private static final String TOTAL = "tat";
    private static Invoice_list_adapter.MyClickListener myClickListener;


    Context context;

    public Invoice_list_adapter(Context c,ArrayList<HashMap<String, String>> myDataset) {
        mDataset = myDataset;
        context=c;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_report_header, parent, false);
            return  new VHHeader(v);
        }
        else if(viewType == TYPE_ITEM)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.monthly_report_row, parent, false);
            return new VHItem(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof VHHeader)
        {
            VHHeader vhHeader = (VHHeader)holder;

        }
        else if(holder instanceof VHItem)
        {

            HashMap<String, String> map = mDataset.get(position-1);
            VHItem VHitem = (VHItem)holder;
            VHitem.invoiceno.setText(map.get(INVOICE_NO));
            VHitem.customername.setText(map.get(CUSTOMER_NAME));
            VHitem.total.setText(map.get(TOTAL));


        }
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        if(position==0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }







    //increasing getItemcount to 1. This will be the row of header.
    @Override
    public int getItemCount() {


        return (mDataset.size())+1;
    }



    class VHHeader extends RecyclerView.ViewHolder{

        TextView itemdesch;

        public VHHeader(View itemView) {
            super(itemView);
            itemdesch = (TextView) itemView.findViewById(R.id.tvh_invoiceno);


        }
    }

    class VHItem extends RecyclerView.ViewHolder implements View
            .OnClickListener{
        TextView invoiceno;
        TextView customername;
        TextView total;

        public VHItem(View itemView) {
            super(itemView);
            invoiceno = (TextView) itemView.findViewById(R.id.tv_invoiceno);
            customername=(TextView)itemView.findViewById(R.id.tv_customername);
            total=(TextView) itemView.findViewById(R.id.tv_total);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }
    public void setOnItemClickListener(Invoice_list_adapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }
    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}
