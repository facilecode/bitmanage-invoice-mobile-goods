package facile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.Units_model;
import facile.invoice.R;

/**
 * Created by pradeep on 10/10/17.
 */
public class UnitslistArrayadapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<Units_model> items;
    private final int mResource;

    public UnitslistArrayadapter(Context context, int resource,
                                 ArrayList<Units_model> objects) {


        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView unitcode = (TextView) view.findViewById(R.id.tv_unitcode);
        TextView unitdesc = (TextView) view.findViewById(R.id.tv_unitdesc);
        TextView unitid = (TextView) view.findViewById(R.id.tv_unitid);

        unitcode.setText(items.get(position).unit_code);
        unitdesc.setText(items.get(position).unit_desc);
        unitid.setText(items.get(position).id);

        unitcode.setTextSize(12);
        unitdesc.setTextSize(12);
        return view;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}

