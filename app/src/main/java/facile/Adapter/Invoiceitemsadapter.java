package facile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.Invoice_Product_model;
import facile.invoice.R;

/**
 * Created by pradeep on 13/03/18.
 */

public class Invoiceitemsadapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<Invoice_Product_model> mDataset;
    Context context;

    public Invoiceitemsadapter(Context c, ArrayList<Invoice_Product_model> myDataset) {
        mDataset = myDataset;
        context=c;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            return  new VHHeader(v);
        }
        else if(viewType == TYPE_ITEM)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.invoiceitemsrow, parent, false);
            return new VHItem(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof VHHeader)
        {
            VHHeader vhHeader = (VHHeader)holder;

        }
        else if(holder instanceof VHItem)
        {

            VHItem VHitem = (VHItem)holder;
            VHitem.itemdesc.setText(mDataset.get(position-1).productdesc);
            VHitem.hsn.setText(mDataset.get(position-1).hsncode);
            VHitem.qty.setText(mDataset.get(position-1).quantity);
            VHitem.rate.setText(mDataset.get(position-1).amount);
            VHitem.amnt.setText(mDataset.get(position-1).grossamount);
            VHitem.uom.setText(mDataset.get(position-1).unit);
            VHitem.disc.setText(mDataset.get(position-1).discount);
            VHitem.gst.setText(mDataset.get(position-1).gstrate);



        }
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        if(position==0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }







    //increasing getItemcount to 1. This will be the row of header.
    @Override
    public int getItemCount() {


        return (mDataset.size())+1;
    }

    class VHHeader extends RecyclerView.ViewHolder{

        TextView itemdesch;

        public VHHeader(View itemView) {
            super(itemView);
            itemdesch = (TextView) itemView.findViewById(R.id.tvh_itemdesc);

        }
    }

    class VHItem extends RecyclerView.ViewHolder{
        TextView itemdesc;
        TextView hsn;
        TextView uom;
        TextView qty;
        TextView rate;
        TextView amnt;
        TextView disc;
        TextView gst;
        public VHItem(View itemView) {
            super(itemView);
            itemdesc = (TextView) itemView.findViewById(R.id.tv_itemdesc);
            hsn=(TextView)itemView.findViewById(R.id.tv_hsncode);
            qty=(TextView) itemView.findViewById(R.id.tv_qty);
            uom=(TextView) itemView.findViewById(R.id.tv_uom);
            rate=(TextView) itemView.findViewById(R.id.tv_rate);
            amnt=(TextView) itemView.findViewById(R.id.tv_amnt);
            disc=(TextView) itemView.findViewById(R.id.tv_discount);
            gst=(TextView) itemView.findViewById(R.id.tv_gst);
        }
    }
}
