package facile.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.Invoice_Product_model;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;

/**
 * Created by pradeep on 14/3/17.
 */

public class additemadapterunreg extends RecyclerView
        .Adapter<additemadapterunreg
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<Invoice_Product_model> mDataset;
    private static MyClickListener myClickListener;
    Context ctx;
    Invoicedatabase invoicedb;


    public static class DataObjectHolder extends RecyclerView.ViewHolder  implements View
            .OnClickListener
    {
        TextView item_code;
        TextView item_desc;
        TextView item_amnt;
        TextView item_disc;
        TextView item_total;
        ImageView del;

        public DataObjectHolder(View itemView) {
            super(itemView);
            item_code = (TextView) itemView.findViewById(R.id.tv_itemcode);
            item_desc = (TextView) itemView.findViewById(R.id.tv_itemdesc);
            item_amnt = (TextView) itemView.findViewById(R.id.tv_itemamnt);
            item_disc = (TextView) itemView.findViewById(R.id.tv_itemdisc);
            item_total=(TextView)itemView.findViewById(R.id.tv_itemtot);
            del =       (ImageView) itemView.findViewById(R.id.iv_delete);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }

    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    public additemadapterunreg(ArrayList<Invoice_Product_model> myDataset, Context c){
        mDataset = myDataset;
        ctx=c;
        invoicedb=new Invoicedatabase(c);
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.addinvoiceitem_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
     /*   holder.label.setText(mDataset.get(position).getId());*/
        holder.item_code.setText(mDataset.get(position).productcode);
        holder.item_desc.setText(mDataset.get(position).productdesc);
        holder.item_amnt.setText(mDataset.get(position).amount+" * " +mDataset.get(position).quantity);
        holder.item_disc.setText(mDataset.get(position).grossamount);
/*        holder.item_total.setText(mDataset.get(position).tat);*/
        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                invoicedb.deleteaddproductitem( mDataset.get(position).productcode);
                deleteItem(holder.getAdapterPosition());
            }
        });

    }

    public void addItem(Invoice_Product_model dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
        notifyItemRangeChanged(index, mDataset.size());
    }

    @Override
    public int getItemCount() {


        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }





}
