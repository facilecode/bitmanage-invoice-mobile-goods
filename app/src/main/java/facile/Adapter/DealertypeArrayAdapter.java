package facile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.Dealer_model;
import facile.invoice.R;

/**
 * Created by pradeep on 10/10/17.
 */
public class DealertypeArrayAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final ArrayList<Dealer_model> items;
    private final int mResource;

    public DealertypeArrayAdapter(Context context, int resource,
                                 ArrayList<Dealer_model> objects) {

        items = objects;
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;

    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView dealerid = (TextView) view.findViewById(R.id.tv_dealerid);
        TextView dealername = (TextView) view.findViewById(R.id.tv_dealertype);
        dealername.setText(items.get(position).dealer_type);
        dealerid.setText(items.get(position).dealer_id);

        dealername.setTextSize(12);
        return view;
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}

