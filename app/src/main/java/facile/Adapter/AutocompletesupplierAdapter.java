package facile.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import facile.Model.Supplier_model;
import facile.invoice.R;

/**
 * Created by pradeep on 7/9/17.
 */

public class AutocompletesupplierAdapter extends ArrayAdapter<Supplier_model> {

    private ArrayList<Supplier_model> list;
    Context context;
    LayoutInflater inflater;
    int resource;

    public AutocompletesupplierAdapter(Context context, int resource, ArrayList<Supplier_model> list) {
        super(context, resource);
        this.context = context;
        this.resource = resource;

        this.list = list;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (view == null) {
            view = inflater.inflate(resource, null);
        }

        Supplier_model model=getItem(position);

        TextView textView = (TextView) view.findViewById(R.id.textView);
        /*TextView textViewColor = (TextView) view.findViewById(R.id.txtColorCode);*/

        textView.setText(model.supplier_name);
        /*textViewColor.setBackgroundColor(model.getColorId());*/

        view.setTag(model);
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Supplier_model) (resultValue)).supplier_name;
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                ArrayList<Supplier_model> suggestions = new ArrayList<>();

                for (Supplier_model color : list) {
                    if (color.supplier_name.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(color);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<Supplier_model>) results.values);
            }
            notifyDataSetChanged();
        }
    };
}
