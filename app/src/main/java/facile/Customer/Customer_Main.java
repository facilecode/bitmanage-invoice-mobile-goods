package facile.Customer;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;

import facile.Adapter.Customer_adapter;
import facile.Master.customer_creation;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;

/**
 * Created by pradeep on 12/2/18.
 */

public class Customer_Main extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Invoicedatabase invoicedb;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    FloatingActionButton fab;
    private Customer_Main.OnFragmentInteractionListener mListener;
    ArrayList<HashMap<String, String>> addaddressHashMap;
    EditText edt_search;

    public Customer_Main() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Customer_Main newInstance(String param1, String param2) {
        Customer_Main fragment = new Customer_Main();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.customer_main, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_add_customer));
        invoicedb = new Invoicedatabase(getContext());
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_customerdetails);
        edt_search=(EditText)view.findViewById(R.id.edt_search);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new Customer_adapter(getContext(), invoicedb.getcustomerdetails());

        mRecyclerView.setAdapter(mAdapter);
        ((Customer_adapter) mAdapter).setOnItemClickListener(new Customer_adapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                invoicedb.deleteaaddshippingaddress();
                customer_creation fragment = new customer_creation();
                FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                int pos=(int)mAdapter.getItemId(position);
                Bundle bundle = new Bundle();
                bundle.putString("CUSTOMER_ID", invoicedb.getcustomerdetails().get(pos).cust_id);
                bundle.putString("CUSTOMER_NAME", invoicedb.getcustomerdetails().get(pos).customer_name);
                bundle.putString("GST_NO", invoicedb.getcustomerdetails().get(pos).gst_no);
                bundle.putString("ADDRESS", invoicedb.getcustomerdetails().get(pos).address);
                bundle.putString("ADDRESS1", invoicedb.getcustomerdetails().get(pos).address1);
                bundle.putString("CITY", invoicedb.getcustomerdetails().get(pos).city);
                bundle.putString("PINCODE", invoicedb.getcustomerdetails().get(pos).pincode);
                bundle.putString("STATE", invoicedb.getcustomerdetails().get(pos).state);
                bundle.putString("STATE_POS", invoicedb.getcustomerdetails().get(pos).state_pos);
                bundle.putString("PHONE_NO", invoicedb.getcustomerdetails().get(pos).phone_no);
                bundle.putString("MAIL_ID", invoicedb.getcustomerdetails().get(pos).mail_id);
                bundle.putString("OPENING_BALANCE", invoicedb.getcustomerdetails().get(pos).openingbal);
                bundle.putString("DRORCR", invoicedb.getcustomerdetails().get(pos).crordr);

                addaddressHashMap = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> addressmap = new HashMap<String, String>();
                addressmap.put("shipping_id", "1");
                addressmap.put("shipping_address", invoicedb.getcustomerdetails().get(pos).shipping_address1);
                addressmap.put("shipping_address1", invoicedb.getcustomerdetails().get(pos).shipping_address2);
                addressmap.put("shipping_city", invoicedb.getcustomerdetails().get(pos).s_city);
                addressmap.put("shipping_state", invoicedb.getcustomerdetails().get(pos).s_state);
                addressmap.put("shipping_country", invoicedb.getcustomerdetails().get(pos).s_country);
                addressmap.put("shipping_pincode", invoicedb.getcustomerdetails().get(pos).s_pincode);
                addaddressHashMap.add(addressmap);
                invoicedb.insertaddshippingaddress(addaddressHashMap);
                bundle.putString("SHIPPING_ID", invoicedb.getcustomerdetails().get(pos).shipping_id);
                bundle.putString("SHIPPPING_ADDRESS1", invoicedb.getcustomerdetails().get(pos).shipping_address1);
                bundle.putString("SHIPPING_ADDRESS2", invoicedb.getcustomerdetails().get(pos).shipping_address2);
                bundle.putString("SHIPPING_CITY", invoicedb.getcustomerdetails().get(pos).s_city);
                bundle.putString("SHIPPING_COUNTRY", invoicedb.getcustomerdetails().get(pos).s_country);
                bundle.putString("SHIPPING_STATE", invoicedb.getcustomerdetails().get(pos).s_state);
                bundle.putString("SHIPPING_PINCODE", invoicedb.getcustomerdetails().get(pos).s_pincode);
                fragment.setArguments(bundle);
                fragmentTransaction.commit();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customer_creation fragment = new customer_creation();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentManager.popBackStack("Customer_Main", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentTransaction.commit();
            }
        });

edt_search.addTextChangedListener(new TextWatcher() {

    @Override
    public void afterTextChanged(Editable s) {}

    @Override
    public void beforeTextChanged(CharSequence s, int start,
                                  int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start,
                              int before, int count) {

            ((Customer_adapter) mAdapter).getFilter().filter(s.toString());

   }
});
        return view;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }




}
