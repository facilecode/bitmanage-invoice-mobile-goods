package facile.creditanddebitnote;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import facile.Adapter.additemadapter;
import facile.Model.Debit_note_details;
import facile.Model.Invoice_Product_model;
import facile.Model.Invoice_calc;
import facile.Model.invoice_customer_model;
import facile.invoice.Additem_fragment;
import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.localdb.Invoicedatabase;
import facile.util.SeparatorDecoration;

/**
 * Created by pradeep on 16/4/18.
 */

public class Debitnote  extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView toasttext;
    View layouttoast;
    RecyclerView mRecyclerView;

    TextView tv_Debitnoteno,txt_customername,txt_invoiceno,txt_gstno,txt_address,txt_creditnoteno,txt_creditnotedate;
    Button btn_additem,btn_submit;
    ArrayList<Invoice_Product_model> invoicproductemodel;
    private RecyclerView.LayoutManager mLayoutManager;
    TextView edt_Debitnotedate;
    additemadapter Itemadapter;
    Invoicedatabase invoicedb;

    String   str_Debitnoteno,str_p_Debitnoteno,str_invoice_id,str_invoice_no,str_customer_name,str_gst_no,str_address,str_phone_no,
            str_mail_id,str_city,str_state,str_state_pos,str_igst,str_pincode ,str_date,str_dateval  ;
     double temp;


    private Debitnote.OnFragmentInteractionListener mListener;

    public Debitnote() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Debitnote newInstance(String param1, String param2) {
        Debitnote fragment = new Debitnote();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.credit_note_business, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_debitnote));
        tv_Debitnoteno=(TextView)view.findViewById(R.id.tv_creditnotenumber);
        edt_Debitnotedate=(TextView)view.findViewById(R.id.edt_creditnotedate);
        txt_customername=(TextView)view.findViewById(R.id.txt_customername);
        txt_invoiceno=(TextView)view.findViewById(R.id.txt_invoiceno);
        txt_gstno=(TextView)view.findViewById(R.id.txt_gstno);
        txt_address=(TextView)view.findViewById(R.id.txt_address);
        txt_creditnoteno=(TextView)view.findViewById(R.id.txt_creditnotenumber);
        txt_creditnotedate=(TextView)view.findViewById(R.id.tv_creditnotedate);
        txt_creditnoteno.setText(R.string.debitote_num);
        txt_creditnotedate.setText(R.string.debitnote_date);
        btn_additem=(Button)view.findViewById(R.id.btn_additem);
        btn_submit=(Button)view.findViewById(R.id.btn_submit);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_invoiceitems);
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        SeparatorDecoration decoration = new SeparatorDecoration(getContext(), Color.GRAY,0.5f);
        mRecyclerView.addItemDecoration(decoration);
        invoicedb = new Invoicedatabase(getContext());
        invoicproductemodel=new ArrayList<Invoice_Product_model>();
        Bundle bundle = this.getArguments();
        if(bundle!=null){
            str_Debitnoteno=bundle.getString("DEBIT_NOTE_NO", "");
            str_customer_name=bundle.getString("CUSTOMER_NAME", "");
            str_invoice_id=bundle.getString("INVOICE_ID", "");
            str_invoice_no = bundle.getString("INVOICE_NO", "");
            str_igst=bundle.getString("IGST", "");
            str_gst_no= bundle.getString("GST_NO", "");
            str_address=bundle.getString("ADDRESS", "");
            str_state=bundle.getString("STATE", "");
            str_pincode=bundle.getString("PINCODE", "");
            str_city=bundle.getString("CITY", "");
            str_phone_no=bundle.getString("PHONE_NO", "");
            str_mail_id=bundle.getString("MAIL_ID", "");
            str_state_pos=bundle.getString("STATE_POS", "");
        }
        str_p_Debitnoteno="DN-"+str_Debitnoteno;
        tv_Debitnoteno.setText(str_p_Debitnoteno);
        txt_customername.setText(str_customer_name);
        txt_invoiceno.setText(str_invoice_no);
        txt_gstno.setText(str_gst_no);
        txt_address.setText(str_address+"\n"+str_city+str_state+str_pincode);
        long date = System.currentTimeMillis();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd:MM:yyyy");
        dateFormat.applyPattern("dd/MM/yy");
        String dateString = dateFormat.format(date);
        edt_Debitnotedate.setText(dateString);
        str_date=dateString;
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy:MM:dd");
        dateFormat1.applyPattern("yyyy/MM/dd");
        str_dateval=dateFormat1.format(date);
        Itemadapter = new additemadapter(invoicedb.getaddproductitems(),getActivity());
        mRecyclerView.setAdapter(Itemadapter);
        ((additemadapter) Itemadapter).setOnItemClickListener(new additemadapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Additem_fragment fragment = new Additem_fragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("prod_code",invoicedb.getaddproductitems().get(position).productcode);
                bundle.putString("prod_desc",invoicedb.getaddproductitems().get(position).productdesc);
                bundle.putString("prod_fees",invoicedb.getaddproductitems().get(position).amount);
                bundle.putString("prod_qty",invoicedb.getaddproductitems().get(position).quantity);
                bundle.putString("prod_disc",invoicedb.getaddproductitems().get(position).discount);
                bundle.putString("prod_disc_type",invoicedb.getaddproductitems().get(position).discountype);
                bundle.putString("prod_unit",invoicedb.getaddproductitems().get(position).unit);
                bundle.putString("gst_rate",invoicedb.getaddproductitems().get(position).gstrate);
                bundle.putString("Igst",str_igst);
                bundle.putString("type","I");
                bundle.putString("business_type","NA");
                    /*    bundle.putString("ITEM_CODE", );
                bundle.putString("ITEM_DESC", invoicedb.getcustomerdetails().get(position).gst_no);
                bundle.putString("SP", invoicedb.getcustomerdetails().get(position).address);
                bundle.putString("QTY", invoicedb.getcustomerdetails().get(position).city);
                bundle.putString("DISCOUNT", invoicedb.getcustomerdetails().get(position).pincode);*/
                fragment.setArguments(bundle);
                fragmentTransaction.commit();


            }
        });



        btn_additem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Additem_fragment fragment = new Additem_fragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.addToBackStack(null);
                Bundle bundle = new Bundle();
                bundle.putString("Igst",str_igst);
                bundle.putString("type","cr_note");
                bundle.putString("business_type","NA");
                fragment.setArguments(bundle);
                fragmentTransaction.commit();
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                int m=invoicedb.getaddproductitems().size();
                for(int i=0;i<m;i++){
                    int x= Integer.parseInt(invoicedb.getaddproductitems().get(i).gstrate);
                    if(x>temp){
                        temp=x;
                    }
                }


                if(invoicedb.getaddproductitemscount()==0){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Add Items can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    return;
                }


                else {


                    String total_before_tax;
                    String total_after_tax;
                    String str_cgst, str_sgst, str_igstval;

                    double totbeforetax = 0, totaftertax = 0, cgst = 0, sgst = 0, igst = 0;


                    for (int i = 0; i < invoicedb.getaddproductitemscount(); i++) {
                        double TOT_BT = totbeforetax + Double.parseDouble(invoicedb.getaddproductitems().get(i).grossamount);
                        double TOT_AT = totaftertax + Double.parseDouble(invoicedb.getaddproductitems().get(i).tat);
                        double CGST = cgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).cgst);
                        double SGST = sgst + Double.parseDouble(invoicedb.getaddproductitems().get(i).sgst);
                        double IGST = igst + Double.parseDouble(invoicedb.getaddproductitems().get(i).igst);
                        totbeforetax = TOT_BT;
                        totaftertax = TOT_AT;
                        cgst = CGST;
                        sgst = SGST;
                        igst = IGST;
                    }


                    total_before_tax=String.valueOf(Math.round((totbeforetax)*100.0)/100.0);
                    total_after_tax=String.valueOf(Math.round((totaftertax)*100.0)/100.0);
                    str_cgst=String.valueOf(Math.round(cgst*100.0)/100.0);
                    str_sgst=String.valueOf(Math.round(sgst*100.0)/100.0);
                    str_igstval=String.valueOf(Math.round(igst*100.0)/100.0);
                    invoice_customer_model invoice_customer_model = new invoice_customer_model
                            ( str_customer_name, str_gst_no, str_address,str_address, str_phone_no, str_mail_id, str_city, str_state, str_pincode, str_state_pos, str_igst);

                    Debit_note_details debit_note=new Debit_note_details(str_invoice_id,str_date,str_Debitnoteno,str_p_Debitnoteno,str_invoice_no,str_dateval);
                    Invoice_calc credit_note_calc=new Invoice_calc("0","0",str_cgst,str_sgst,str_igstval,total_before_tax,total_after_tax);

                    Intent myIntent = new Intent(getActivity(),Credit_note_preview.class);
                    myIntent.putExtra("Debit","D");
                    myIntent.putExtra("customer",invoice_customer_model);
                    myIntent.putExtra("debit_note",debit_note);
                    myIntent.putExtra("credit_note_calc",credit_note_calc);
                    getActivity().startActivity(myIntent);

                }
            }
        });




        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}


