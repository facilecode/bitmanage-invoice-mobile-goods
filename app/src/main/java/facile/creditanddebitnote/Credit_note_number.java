package facile.creditanddebitnote;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import facile.invoice.MainActivity;
import facile.invoice.R;
import facile.util.Constants;
import facile.util.errorhandler;
import facile.util.prefManager;

/**
 * Created by pradeep on 11/4/18.
 */

public class Credit_note_number  extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String url;
    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    prefManager prefs;
    public static final String UTF8_BOM = "\uFEFF";

   Button btn_submit;
    EditText edt_invoiceno;
    TextView toasttext;

    View layouttoast;

    String str_edt_invoice_no,str_creditnoteno,str_invoice_id,str_invoice_no,str_customer_name,str_gst_no,str_address,str_phone_no,
         str_mail_id,str_city,str_state,str_state_pos,str_igst,str_pincode   ;


   private Credit_note_number.OnFragmentInteractionListener mListener;

    public Credit_note_number() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PeopleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Credit_note_number newInstance(String param1, String param2) {
        Credit_note_number fragment = new Credit_note_number();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.creditnotenumber, container, false);
        ((MainActivity) getActivity()).setooltitle(getString(R.string.title_creditnote));
        layouttoast = inflater.inflate(R.layout.customtoast,(ViewGroup)view.findViewById(R.id.custom_toast_layout));
        toasttext = (TextView) layouttoast.findViewById(R.id.custom_toast_message);
        prefs=new prefManager(getContext());
        edt_invoiceno=(EditText)view.findViewById(R.id.edt_invoiceno);
        btn_submit=(Button)view.findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_edt_invoice_no=edt_invoiceno.getText().toString();

                if(TextUtils.isEmpty(str_edt_invoice_no)){
                    Toast toast = new Toast(getContext());
                    toast.setDuration(Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toasttext.setText("Invoice number can't be empty");
                    toast.setView(layouttoast);
                    toast.show();
                    return;

                }

                url = Constants.CREDIT_NOTE_NUMBER;
                requestQueue = Volley.newRequestQueue(getContext());
                progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
                progressDialog.setMessage("Validating invoice no...");
                progressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    String m=response.toString();
                                    String x=     removeUTF8BOM(m);
                                    JSONObject jsonRequest = new JSONObject(x);
                                    String statuscode = jsonRequest.getString("status_code");

                                    if (statuscode.equals("200")) {
                                        progressDialog.dismiss();
                                        str_creditnoteno= jsonRequest.getString("credit_note_no");
                                        JSONArray invoice_details = jsonRequest.getJSONArray("invoice_details");

                                        for (int iStatusList = 0; iStatusList < invoice_details.length(); iStatusList++) {
                                            JSONObject invoice = invoice_details.getJSONObject(iStatusList);
                                           str_invoice_id= invoice.getString("id");
                                           str_invoice_no= invoice.getString("p_invoice_no_s");
                                            str_customer_name=invoice.getString("customer_name");
                                           str_gst_no= invoice.getString("gst_no");
                                           str_address=  invoice.getString("address");
                                           str_phone_no= invoice.getString("phone_no");
                                           str_mail_id= invoice.getString("mail_id");
                                           str_city= invoice.getString("city");
                                           str_state= invoice.getString("state");
                                            str_state_pos=invoice.getString("state_pos");
                                           str_igst= invoice.getString("igst");
                                           str_pincode=invoice.getString("pincode");

                                        }


                                        Creditnote fragment = new Creditnote();
                                        FragmentManager fragmentManager =(getActivity()).getSupportFragmentManager();
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        fragmentTransaction.replace(R.id.container_body, fragment);
                                        fragmentTransaction.addToBackStack(null);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("CREDIT_NOTE_NO", str_creditnoteno);
                                        bundle.putString("INVOICE_ID", str_invoice_id);
                                        bundle.putString("INVOICE_NO", str_invoice_no);
                                        bundle.putString("IGST", str_igst);
                                        bundle.putString("CUSTOMER_NAME", str_customer_name);
                                        bundle.putString("GST_NO", str_gst_no);
                                        bundle.putString("ADDRESS", str_address);
                                        bundle.putString("CITY", str_city);
                                        bundle.putString("PINCODE",str_pincode);
                                        bundle.putString("STATE", str_state);
                                        bundle.putString("STATE_POS", str_state_pos);
                                        bundle.putString("PHONE_NO",str_phone_no);
                                        bundle.putString("MAIL_ID",str_mail_id);
                                        fragment.setArguments(bundle);
                                        fragmentTransaction.commit();

                                    }
                                    else   if (statuscode.equals("600")){


                                        Toast toast = new Toast(getContext());
                                        toast.setDuration(Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                                        toasttext.setText(jsonRequest.getString("status"));
                                        toast.setView(layouttoast);
                                        progressDialog.dismiss();
                                        toast.show();


                                    }


                                } catch (Exception e) {

                                    progressDialog.dismiss();


                                }
                            }
                        },


                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                        errorhandler e=new errorhandler();
                        String msg=e.errorhandler(error);
                        Toast toast = new Toast(getContext());
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toasttext.setText(msg);
                        toast.setView(layouttoast);
                        progressDialog.dismiss();
                        toast.show();


                            }
                        }) {


                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id",prefs.getUserId());
                        params.put("invoice_no",str_edt_invoice_no);
                        return params;
                    }


                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("content-type","application/x-www-form-urlencoded; charset=UTF-8");
                        return params;
                    }

                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,-1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(stringRequest);


            }
        });



        return view;

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private static String removeUTF8BOM(String s) {
        if (s.startsWith(UTF8_BOM)) {
            s = s.substring(1);
        }
        return s;
    }


}


