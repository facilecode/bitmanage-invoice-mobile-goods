BEGIN TRANSACTION;


CREATE TABLE Supplier_details
(
    supplier_id INTEGER,
    supplier_name TEXT,
    Gst_no TEXT,
    user_id TEXT,
    address TEXT,
    address1 TEXT,
    city TEXT,
    state TEXT,
    Pincode TEXT,
    phone_no TEXT,
    mail_id TEXT,
    state_pos TEXT,
    address_line_1 TEXT,
    address_line_2 TEXT,
    opening_balance TEXT,
    opening_balance_crordr TEXT
);

COMMIT TRANSACTION;
